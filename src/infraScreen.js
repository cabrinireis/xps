var gInfra_inputAnalog   = document.querySelectorAll('.input-switch.eacard');
var gInfra_inputInv      = document.querySelectorAll('.input-switch.eicard');
var gInfra_inputName     = document.querySelectorAll('.input-custom.iname');
var gInfra_inputVal      = document.querySelectorAll('.iiaval');
var gInfra_inputMin      = document.querySelectorAll('.input-custom-config.iemin');
var gInfra_inputMax      = document.querySelectorAll('.input-custom-config.iemax');
var gInfra_inputSetP     = document.querySelectorAll('.input-custom-config.iespo');
var gInfra_inputHyst     = document.querySelectorAll('.input-custom-config.iehys');
var gInfra_inputUnit     = document.querySelectorAll('.input-custom-config.ieuni');
var gInfra_inputDecP     = document.querySelectorAll('.input-custom-config.iedpo');

var gInfra_relayName     = document.querySelectorAll('.form-control.input-sm.irlnm');
var gInfra_relayOn       = document.querySelectorAll('.input-switch.rlcard');
var gInfra_relayInv      = document.querySelectorAll('.input-switch.rlicard');

var gInfra_temperInv     = document.querySelectorAll('.input-switch.iticard');
var gInfra_temperName    = document.querySelectorAll('.input-custom.itname');
var gInfra_temperVal     = document.querySelectorAll('.itval');
var gInfra_temperSP      = document.querySelectorAll('.input-custom-config.itsp');
var gInfra_temperHyst    = document.querySelectorAll('.input-custom-config.ithys');

var gInfra_inputError    = document.querySelectorAll('.entrada-result');

function local_enabeInfraEntry(idx)
{
    gInfra_inputMin[idx].disabled  = false;
    gInfra_inputMax[idx].disabled  = false;
    gInfra_inputSetP[idx].disabled = false;
    gInfra_inputHyst[idx].disabled = false;
    gInfra_inputUnit[idx].disabled = false;
    gInfra_inputDecP[idx].disabled = false;
};
//-------------------------------------------------------------------------------------------------

function local_disabeInfraEntry(idx)
{
    gInfra_inputDecP[idx].disabled = true;
    gInfra_inputMin[idx].disabled = true;
    gInfra_inputMax[idx].disabled = true;
    gInfra_inputSetP[idx].disabled = true;
    gInfra_inputHyst[idx].disabled = true;
    gInfra_inputUnit[idx].disabled = true;
};
//-------------------------------------------------------------------------------------------------

function local_infra_analogChange(idx)
{
    local_disabeInfraEntry(idx);
    if(gInfra_inputAnalog[idx].checked){
        local_enabeInfraEntry(idx);
    }
};
//-------------------------------------------------------------------------------------------------

function local_infra_changeAna1()
{
    local_infra_analogChange(0);
};
//-------------------------------------------------------------------------------------------------

function local_infra_changeAna2()
{
    local_infra_analogChange(1);
};
//-------------------------------------------------------------------------------------------------

function local_infra_changeAna3()
{
    local_infra_analogChange(2);
};
//-------------------------------------------------------------------------------------------------

function local_infra_changeAna4()
{
    local_infra_analogChange(3);
};
//-------------------------------------------------------------------------------------------------

function local_infra_changeAna5()
{
    local_infra_analogChange(4);
};
//-------------------------------------------------------------------------------------------------


function fbyteToBits(v)
{
    var r = [0,0,0,0,0,0,0,0];

    if(v >= 256){
        return [];
    };

    if(v&128) r[7]=1;
    if(v&64)  r[6]=1;
    if(v&32)  r[5]=1;
    if(v&16)  r[4]=1;
    if(v&8)   r[3]=1;
    if(v&4)   r[2]=1;
    if(v&2)   r[1]=1;
    if(v&1)   r[0]=1;

    return r;
};
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

function infra_clearBorders()
{
    let i=0;

    for(i=0; i<gInfra_inputName.length; i++){
        gInfra_inputName[i].classList.remove('inputChangeBorder');
        gInfra_inputName[i].classList.remove('inputErrorBorder');
    }

    for(i=0; i<gInfra_relayName.length; i++){
        gInfra_relayName[i].classList.remove('inputChangeBorder');
        gInfra_relayName[i].classList.remove('inputErrorBorder');
    }

    for(i=0; i<gInfra_temperName.length; i++){
        gInfra_temperName[i].classList.remove('inputChangeBorder');
        gInfra_temperName[i].classList.remove('inputErrorBorder');
    }
}
//-------------------------------------------------------------------------------------------------



function infraGetLiveValues()
{
    myHttpGet('inframonent.txt',fInfraMonInputCallback);
}
//-------------------------------------------------------------------------------------------------

// Format: X*vvvv*A
// X: 0=analog, 1=digital
// vvvv: numeric value
// A: 0=Alarm On, 1= Alarm off 
function fInfraMonInputCallback(text)
{
    let offset = 0;
    let decimalPoint = [];
    let analogReads = [];
    let temperReads = [];

    
    values = text.split(',');
    if(values.length != 14){
        pLog("String Error: " + values.length);
        return;
    }

    alarms = values[offset++]; 	    // Alarm 
    analogmode = values[offset++];  // Analog mode
    decimalPoint[0] = parseInt(values[offset++],16);
    decimalPoint[1] = parseInt(values[offset++],16);
    decimalPoint[2] = parseInt(values[offset++],16);
    decimalPoint[3] = parseInt(values[offset++],16);
    decimalPoint[4] = parseInt(values[offset++],16);
    analogReads[0]  = parseInt(values[offset++],16);
    analogReads[1]  = parseInt(values[offset++],16);
    analogReads[2]  = parseInt(values[offset++],16);
    analogReads[3]  = parseInt(values[offset++],16);
    analogReads[4]  = parseInt(values[offset++],16);
    temperReads[0]  = parseInt(values[offset++],16);
    temperReads[1]  = parseInt(values[offset++],16);

    for(let i=0; i<7; i++){
        alarms[i] == '1'? 
            gInfra_inputError[i].classList.add('infra-input-error'):
            gInfra_inputError[i].classList.remove('infra-input-error');
    }

    for (let i=0; i<5; i++){
        gInfra_inputVal[i].innerHTML = (analogReads[i]/(10**decimalPoint[i])).toFixed(decimalPoint[i]);
    }

    gInfra_temperVal[0].innerHTML = temperReads[0]==0xff36?'Invertido':temperReads[0]==0xff37?'Sem Sen':temperReads[0];
    gInfra_temperVal[1].innerHTML = temperReads[1]==0xff36?'Invertido':temperReads[1]==0xff37?'Sem Sen':temperReads[1];

    pLog(text);
};
//-------------------------------------------------------------------------------------------------


function local_getBitsToDec(arrayIn)
{
    let ret = 0;

    for(i=0; i<arrayIn.length;i++){
        ret |= (arrayIn[i].checked?1:0) << i;
    }

    return ret;
}
//-------------------------------------------------------------------------------------------------

function local_setDecToBits(arrayIn,value)
{
    for(i=0; i<arrayIn.length;i++){
        arrayIn[i].checked = false;
        if (value & (1<<i)){
            arrayIn[i].checked = true;
        }
    }
}
//-------------------------------------------------------------------------------------------------

function local_getLabelNames(arrayIn)
{
    let ret = "";

    for(i=0; i<arrayIn.length;i++){
        ret += arrayIn[i].value;
        ret += ',';
    }

    return ret.slice(0,-1); 
}
//-------------------------------------------------------------------------------------------------

function local_setLabelByArray(fieldIn,arrayIn)
{
    if(fieldIn.length != arrayIn.length){
        pLog("Format Error.");
        return;
    }

    for(i=0; i<arrayIn.length;i++){
        fieldIn[i].value = arrayIn[i];
    }
}
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


function local_getInputAnalogMode()
{
    return local_getBitsToDec(gInfra_inputAnalog);
}
//-------------------------------------------------------------------------------------------------

function local_setInputAnalogMode(value)
{
    local_setDecToBits(gInfra_inputAnalog,value);
    for(var i=0; i<gInfra_inputAnalog.length; i++){
        local_disabeInfraEntry(i);
        if(value & (1<<i)){
            local_enabeInfraEntry(i);
        }
    }
}
//-------------------------------------------------------------------------------------------------


function local_getInputInv()
{
    return local_getBitsToDec(gInfra_inputInv);    
}
//-------------------------------------------------------------------------------------------------

function local_setInputInv(value)
{
    local_setDecToBits(gInfra_inputInv,value);
}
//-------------------------------------------------------------------------------------------------


function local_getRelayOn()
{
    return local_getBitsToDec(gInfra_relayOn);    
}
//-------------------------------------------------------------------------------------------------

function local_setRelayOn(value)
{
    local_setDecToBits(gInfra_relayOn,value);
}
//-------------------------------------------------------------------------------------------------


function local_getRelayInv()
{
    return local_getBitsToDec(gInfra_relayInv);    
}
//-------------------------------------------------------------------------------------------------

function local_setRelayInv(value)
{
    local_setDecToBits(gInfra_relayInv,value);
}
//-------------------------------------------------------------------------------------------------


function local_getTemperInv()
{
    return local_getBitsToDec(gInfra_temperInv);    
}
//-------------------------------------------------------------------------------------------------

function local_setTemperInv(value)
{
    local_setDecToBits(gInfra_temperInv,value);
}
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


function local_getInputNames()
{
    return local_getLabelNames(gInfra_inputName);
}
//-------------------------------------------------------------------------------------------------

function local_setInputNames(names)
{
    local_setLabelByArray(gInfra_inputName,names);
}
//-------------------------------------------------------------------------------------------------

function local_getRelayNames()
{
    return local_getLabelNames(gInfra_relayName);
}
//-------------------------------------------------------------------------------------------------

function local_setRelayNames(names)
{
    local_setLabelByArray(gInfra_relayName,names);
}
//-------------------------------------------------------------------------------------------------

function local_getTemperNames()
{
    return local_getLabelNames(gInfra_temperName);
}
//-------------------------------------------------------------------------------------------------

function local_setTemperNames(names)
{
    local_setLabelByArray(gInfra_temperName,names);
}
//-------------------------------------------------------------------------------------------------


//--------------------------
//------ Parameters --------
//--------------------------
function local_getInputParamnsMin()
{
    return local_getLabelNames(gInfra_inputMin);
}
//-------------------------------------------------------------------------------------------------

function local_setInputParamnsMin(arrayIn)
{
    local_setLabelByArray(gInfra_inputMin,arrayIn);    
}
//-------------------------------------------------------------------------------------------------

function local_getInputParamnsMax()
{
    return local_getLabelNames(gInfra_inputMax);
}
//-------------------------------------------------------------------------------------------------

function local_setInputParamnsMax(arrayIn)
{
    local_setLabelByArray(gInfra_inputMax,arrayIn);    
}
//-------------------------------------------------------------------------------------------------

function local_getInputParamnsSetP()
{
    return local_getLabelNames(gInfra_inputSetP);
}
//-------------------------------------------------------------------------------------------------

function local_setInputParamnsSetP(arrayIn)
{
    local_setLabelByArray(gInfra_inputSetP,arrayIn);    
}
//-------------------------------------------------------------------------------------------------

function local_getInputParamnsHyst()
{
    return local_getLabelNames(gInfra_inputHyst);
}
//-------------------------------------------------------------------------------------------------

function local_setInputParamnsHyst(arrayIn)
{
    local_setLabelByArray(gInfra_inputHyst,arrayIn);    
}
//-------------------------------------------------------------------------------------------------

function local_getInputParamnsUnit()
{
    return local_getLabelNames(gInfra_inputUnit);
}
//-------------------------------------------------------------------------------------------------

function local_setInputParamnsUnit(arrayIn)
{
    local_setLabelByArray(gInfra_inputUnit,arrayIn);    
}
//-------------------------------------------------------------------------------------------------

function local_getInputParamnsDecP()
{
    return local_getLabelNames(gInfra_inputDecP);
}
//-------------------------------------------------------------------------------------------------

function local_setInputParamnsDecP(arrayIn)
{
    local_setLabelByArray(gInfra_inputDecP,arrayIn);    
}
//-------------------------------------------------------------------------------------------------


function local_getTemperParamnsSP()
{
    return local_getLabelNames(gInfra_temperSP);
}
//-------------------------------------------------------------------------------------------------

function local_setTemperParamnsSP(arrayIn)
{
    local_setLabelByArray(gInfra_temperSP,arrayIn);    
}
//-------------------------------------------------------------------------------------------------


function local_getTemperParamnsHY()
{
    return local_getLabelNames(gInfra_temperHyst);
}
//-------------------------------------------------------------------------------------------------

function local_setTemperParamnsHY(arrayIn)
{
    local_setLabelByArray(gInfra_temperHyst,arrayIn);    
}
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------


function local_decStrToHexStr(strIn)
{
    let ttt = strIn.split(',');
    let ret = "";

    for(let i=0; i<ttt.length; i++){
        if(ttt[i] == ''){
            ttt[i] = '0'
        } 
        ret += parseInt(ttt[i]).toString(16);
        ret += ",";
    }

    return ret.slice(0,-1);
};
//-------------------------------------------------------------------------------------------------



//---------------------------
//---   Botao de Ler      ---
//---------------------------

function fInfraReadButton()
{
    myHttpGet('ifRalB.txt',callback_fInfraReadButton);    
};
//-------------------------------------------------------------------------------------------------

function callback_fInfraReadButton(text)
{
    pLog(text);

    let sets = text.split('*');

    if(sets.length != 53){
        console.log("Error on string format.");
        return;
    }

    let inputNames = sets.slice(0,5);
    let relayNames = sets.slice(5,12);
    let tmperNames = sets.slice(12,14);
    let bitsRelayOn  = parseInt(sets[14],16);
    let bitsRelayInv  = parseInt(sets[15],16);
    let bitsInputMode   = parseInt(sets[16],16);
    let bitsInputInv  = parseInt(sets[17],16);
    let bitsTemperInv = parseInt(sets[18],16);

    let vals    = sets.slice(19,53);
    let valsMin = vals.slice(0,5);
    let valsMax = vals.slice(5,10);
    let valsSp  = vals.slice(10,15);
    let valsHys = vals.slice(15,20);
    let valsUn  = vals.slice(20,25);
    let valsPd  = vals.slice(25,30);
    let valsTempSp  = vals.slice(30,32);    
    let valsTempHy  = vals.slice(32,34);    

    for(let i=0; i<5; i++){
        valsMin[i] = parseInt(valsMin[i],16);
        valsMax[i] = parseInt(valsMax[i],16);
        valsSp[i]  = parseInt(valsSp[i],16);
        valsHys[i] = parseInt(valsHys[i],16);
        valsPd[i]  = parseInt(valsPd[i],16);
    }

    for(let i=0; i<2; i++){
        valsTempSp[i] = parseInt(valsTempSp[i],16);
        valsTempHy[i] = parseInt(valsTempHy [i],16);
    }

    local_setRelayNames (relayNames);
    local_setInputNames (inputNames);
    local_setTemperNames(tmperNames);    

    local_setInputAnalogMode(bitsInputMode);
    local_setInputInv(bitsInputInv);
    local_setRelayOn(bitsRelayOn);
    local_setRelayInv(bitsRelayInv);
    local_setTemperInv(bitsTemperInv);

    local_setInputParamnsMin(valsMin);
    local_setInputParamnsMax(valsMax);
    local_setInputParamnsSetP(valsSp);
    local_setInputParamnsHyst(valsHys);
    local_setInputParamnsUnit(valsUn);
    local_setInputParamnsDecP(valsPd);
    local_setTemperParamnsSP(valsTempSp);
    local_setTemperParamnsHY(valsTempHy);

    for(let i=0; i<relayNames.length; i++){
        monit_setOutputLabels(i,relayNames[i]);
    }
    
    for(let i=0; i<inputNames.length; i++){
        alarm_setLabels(i,inputNames[i]);
        monit_setInputLabels(i,inputNames[i]);
    }

    for(let i=0; i<tmperNames.length; i++){
        alarm_setLabels(i+5,tmperNames[i]);
        monit_setTemperaLabels(i,tmperNames[i]);
    }

    // infra_clearBorders();
   
    console.log("OK");
};
//-------------------------------------------------------------------------------------------------

//---------------------------
//---   Botao de Salvar   ---
//---------------------------
function fInfraSaveButton()
{
    let s = "";

    // Labels
    s+= local_getInputNames();
    s+= ",";
    s+= local_getRelayNames();
    s+= ",";
    s+= local_getTemperNames();
    s+= ",";
 
    // Inputs
    s += local_decStrToHexStr(local_getInputParamnsMin());
    s+= ",";
    s += local_decStrToHexStr(local_getInputParamnsMax());
    s+= ",";
    s += local_decStrToHexStr(local_getInputParamnsSetP());
    s+= ",";
    s += local_decStrToHexStr(local_getInputParamnsHyst());
    s+= ",";
    s += local_getInputParamnsUnit();
    s+= ",";
    s += local_decStrToHexStr(local_getInputParamnsDecP()); 
    s+= ",";

    // Temperature
    s += local_decStrToHexStr(local_getTemperParamnsSP()); 
    s+= ",";
    s += local_decStrToHexStr(local_getTemperParamnsHY()); 
    s+= ",";

    // Buttons
    s += local_getInputAnalogMode().toString(16);
    s += ","
    s += local_getInputInv().toString(16);
    s += ","
    s += local_getRelayOn().toString(16);
    s += ","
    s += local_getRelayInv().toString(16);
    s += ","
    s += local_getTemperInv().toString(16);


    // Replace white spaces by "?"
    s = s.replace(/ /g, "?");

    pLog(s);
    myHttpPost('paraminfra=='+ s, fInfraPostCallback);
};
//-------------------------------------------------------------------------------------------------

function fInfraPostCallback(text)
{
    if(text == "OK"){
        xpsAlert("Alterações salvas com sucesso.");
    }

    infra_clearBorders()
    fInfraReadButton();
    pLog(text);
};
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

function Infra_Initialize()
{
    gInfra_inputAnalog[0].addEventListener("change", local_infra_changeAna1);
    gInfra_inputAnalog[1].addEventListener("change", local_infra_changeAna2);
    gInfra_inputAnalog[2].addEventListener("change", local_infra_changeAna3);
    gInfra_inputAnalog[3].addEventListener("change", local_infra_changeAna4);
    gInfra_inputAnalog[4].addEventListener("change", local_infra_changeAna5);    
};

Infra_Initialize();