function chart (inputArgs) {
    var defArgs = {
        textHeight: 12,
        textPad: 10,
        grid: true,
        xGrids: 6,
        yGrids: 5,
        xAxisLabel: false,
        yAxisLabel: false,
        timeSeries: false,
        linkGaps: true,
        gapWidth: 0,
        lineColor: 'blue',
        legend: undefined,
        range: undefined,
        toggleData: []
    };
    var args = Object.assign({}, defArgs, inputArgs);
    if (!args.data) {
        return;
    }
    if (!args.id) {
        throw 'No element designated';
    }

    var singleAxis = (typeof args.yAxisLabel === 'string' || args.yAxisLabel instanceof String) || (args.yAxisLabel instanceof Array && args.yAxisLabel.length === 1);
    var doubleAxis = args.yAxisLabel instanceof Array && args.yAxisLabel.length === 2;

    if (!singleAxis && !doubleAxis) {
        throw 'One or two axis';
    }

    var canvas = getElid(args.id);
    var context = canvas.getContext('2d');

    if (args.data.length > 0 && args.data[0].y instanceof Array) {
        for (var i = 0; i < args.data[0].y.length; i++) {
            args.toggleData.push(true);
        }
    } else {
        args.toggleData.push(true);
    }
    var clickRegions = [];

    var draw = function() {
        context.clearRect(0, 0, canvas.width, canvas.height);
        context.save(); // salvando antes do translate
        context.translate(0.5, 0.5); // crisp lines
        clickRegions = [];

        // Limites do Eixo X
        var minX = 0, maxX = 100;
        if (args.range instanceof Array && args.range.length === 2) {
            minX = args.range[0];
            maxX = args.range[1];
        } else {
            minX = args.data.reduce(function(min, d) { return Math.min(d.x, min) }, Number.MAX_VALUE);
            maxX = args.data.reduce(function(max, d) { return Math.max(d.x, max) }, Number.MIN_VALUE);
        }

        // Limites do Eixo Y
        var minY = 0, maxY = 100, minY2 = 0, maxY2 = 100;
        if (args.data.length > 0) {
            if (singleAxis){
                maxY = args.data.reduce(function(max, d) {
                    return d.y instanceof Array ?
                        d.y.reduce(function(_max, y) {return Math.max(y, _max)}, max)
                        : Math.max(d.y, max);
                }, Number.MIN_VALUE),
                minY = args.data.reduce(function(min, d) {
                    return d.y instanceof Array ?
                        d.y.reduce(function(_min, y) {return Math.min(y, _min)}, min)
                        : Math.min(d.y, min);
                }, Number.MAX_VALUE);
                if (maxY % 10 === 0){
                    maxY += 1;
                }
                if (minY % 10 === 0){
                    minY -= 1;
                }
                // if (maxY - minY > 0 && maxY - minY < 1.5) {
                //     maxY = 0.05 * Math.ceil(maxY/0.05);
                //     minY = 0.05 * Math.floor(minY/0.05);
                // } else if (maxY - minY >= 1.5) {
                maxY = 10 * Math.ceil(maxY/10);
                minY = 10 * Math.floor(minY/10);
                // }
            } else if (doubleAxis) {
                maxY = args.data.reduce(function(max, d) {
                    return Math.max(d.y[0], max);
                }, Number.MIN_VALUE),
                minY = args.data.reduce(function(min, d) {
                    return Math.min(d.y[0], min);
                }, Number.MAX_VALUE)
                maxY2 = args.data.reduce(function(max, d) {
                    return Math.max(d.y[1], max);
                }, Number.MIN_VALUE),
                minY2 = args.data.reduce(function(min, d) {
                    return Math.min(d.y[1], min);
                }, Number.MAX_VALUE);
                if (maxY % 10 === 0){
                    maxY += 1;
                }
                if (minY % 10 === 0){
                    minY -= 1;
                }
                // if (maxY - minY > 0 && maxY - minY < 1.5) {
                //     maxY = 0.05 * Math.ceil(maxY/0.05);
                //     minY = 0.05 * Math.floor(minY/0.05);
                // } else if (maxY - minY >= 1.5) {
                maxY = 10 * Math.ceil(maxY/10);
                minY = 10 * Math.floor(minY/10);
                // }
                if (doubleAxis) {
                    if (maxY2 % 10 === 0){
                        maxY2 += 1;
                    }
                    if (minY2 % 10 === 0){
                        minY2 -= 1;
                    }
                    // if (maxY2 - minY2 > 0 && maxY2 - minY2 < 1.5) {
                    //     maxY2 = 0.05 * Math.ceil(maxY2/0.05);
                    //     minY2 = 0.05 * Math.floor(minY2/0.05);
                    // } else if (maxY2 - minY2 >= 1.5) {
                    maxY2 = 10 * Math.ceil(maxY2/10);
                    minY2 = 10 * Math.floor(minY2/10);
                    // }
                }
            }
        }

        // Constantes
        var pt = 2*args.textPad +
                (args.legend ? args.textHeight : 0), // padding top
            pr = 7*args.textPad, // padding right
            pb = 2*args.textPad + args.textHeight +
                (args.xAxisLabel ? args.textPad + args.textHeight : 0), // padding bottom
            pl = 7*args.textPad, // padding left
            w = canvas.width - pl - pr,
            h = canvas.height - pt - pb,
            gX = (maxX - minX) / args.xGrids,
            gY = (maxY - minY) / args.yGrids,
            gY2 = 0,
            dX = (maxX - minX) / w,
            dY = (maxY - minY) / h,
            dY2 = 0;
        if (doubleAxis) {
            gY2 = (maxY2 - minY2) / args.yGrids;
            dY2 = (maxY2 - minY2) / h;
        }

        context.fillStyle = '#fff';
        context.fillRect(0, 0, canvas.width, canvas.height);
        context.fillStyle = '#000';
        context.font = args.textHeight + 'px "Helvetica Neue", Helvetica, Arial, sans-serif';

        //Grid X
        context.strokeStyle = '#ddd';
        context.textAlign = 'center';
        context.textBaseline = 'top';
        var range = maxX - minX;
        // text
        if (range === 0) { // minX === maxX, that is, only 1 point
            var v = minX;
            if (args.timeSeries) {
                var d = new Date(v);
                var text = '';
                if (range > 86400000) {
                    text += d.getUTCDate().toString().padStart(2, '0')
                        + '/'
                        + (d.getUTCMonth()+1).toString().padStart(2, '0')
                        + ' ';
                }
                text += d.getUTCHours().toString().padStart(2, '0')
                    + ':'
                    + d.getUTCMinutes().toString().padStart(2, '0');
                if (range < 3600000) {
                    text += ':'
                        + d.getUTCSeconds().toString().padStart(2, '0');
                }
                context.fillText(
                    text,
                    pl + w/2,
                    h + pt + args.textPad
                );
            } else {
                context.fillText(
                    v,
                    pl + w/2,
                    h + pt + args.textPad
                );
            }
        } else {
            for (var i = 0; i <= args.xGrids; i++) {
                var v;
                if (maxY - minY < 1.5) {
                    v = Math.round((minX + i*gX)*100)/100;
                } else {
                    v = Math.round(minX + i*gX);
                }
                if (args.timeSeries) {
                    var d = new Date(v);
                    var text = '';
                    if (range > 86400000) {
                        text += d.getUTCDate().toString().padStart(2, '0')
                            + '/'
                            + (d.getUTCMonth()+1).toString().padStart(2, '0')
                            + ' ';
                    }
                    text += d.getUTCHours().toString().padStart(2, '0')
                        + ':'
                        + d.getUTCMinutes().toString().padStart(2, '0');
                    if (range < 3600000) {
                        text += ':'
                            + d.getUTCSeconds().toString().padStart(2, '0');
                    }
                    context.fillText(text, pl + i*gX/dX, h + pt + args.textPad);
                } else {
                    context.fillText(
                        v,
                        pl + i*gX/dX,
                        h + pt + args.textPad
                    );
                }
            }
        }
        // lines
        if (args.grid) {
            if (dX === 0) {
                context.beginPath();
                context.moveTo(pl + w/2, pt);
                context.lineTo(pl + w/2, h + pt);
                context.stroke();
            } else {
                for (var i = 0; i <= args.xGrids; i++) {
                    context.beginPath();
                    context.moveTo(pl + i*gX/dX, pt);
                    context.lineTo(pl + i*gX/dX, h + pt);
                    context.stroke();
                }
            }
        }

        //Grid Y
        context.textBaseline = 'middle';
        if (dY === 0) { // minY === maxY
            var v = minY;
            context.textAlign = 'right';
            context.fillText(
                v,
                pl - args.textPad,
                pt + h/2
            );
        } else {
            // left axis
            for (var i = 0; i <= args.yGrids; i++) {
                var v;
                if (maxY - minY < 1.5) {
                    v = Math.round((minY + i*gY)*100)/100;
                } else {
                    v = Math.round(minY + i*gY);
                }
                context.textAlign = 'right';
                context.fillText(
                    v,
                    pl - args.textPad,
                    h + pt - (i* (h/args.yGrids))
                );
            }
        }
        if (doubleAxis) {
            if (dY2 === 0) { // minY2 === maxY2
                var v = minY2;
                context.textAlign = 'left';
                context.fillText(
                    v,
                    pl + w + args.textPad,
                    pt + h/2
                );
            } else {
                // right axis
                for (var i = 0; i <= args.yGrids; i++) {
                    var v2;
                    if (maxY2 - minY2 < 1.5) {
                        v2 = Math.round((minY2 + i*gY2)*100)/100;
                    } else {
                        v2 = Math.round(minY2 + i*gY2);
                    }
                    context.textAlign = 'left';
                    context.fillText(
                        v2,
                        pl + w + args.textPad,
                        h + pt - (i* (h/args.yGrids))
                    );
                }
            }
        }
        // lines
        if (args.grid) {
            if ((singleAxis && dY === 0) || (doubleAxis && dY === 0 & dY2 === 0)) {
                context.beginPath();
                context.moveTo(pl, pt);
                context.lineTo(pl + w, pt);
                context.stroke();

                context.beginPath();
                context.moveTo(pl, pt + h/2);
                context.lineTo(pl + w, pt + h/2);
                context.stroke();

                context.beginPath();
                context.moveTo(pl, pt + h);
                context.lineTo(pl + w, pt + h);
                context.stroke();
            } else {
                for (var i = 0; i <= args.yGrids; i++) {
                    context.beginPath();
                    context.moveTo(pl, h + pt - (i* (h/args.yGrids)));
                    context.lineTo(pl + w, h + pt - (i* (h/args.yGrids)));
                    context.stroke();
                }
            }
        }

        // Legenda
        if (args.legend) {
            var square = 10;
            var ls = args.legend.length;
            var L = ls*square + (5*ls - 4)*args.textPad/2 +
                args.legend.reduce(function(length, str) {return length + context.measureText(str).width}, 0);
            var x = canvas.width/2 - L/2;
            for (var i = 0; i < ls; i++) {
                x += i > 0 ? 2*args.textPad : 0;
                context.fillStyle = args.lineColor[i];
                context.fillRect(x, args.textPad, square, square);
                clickRegions.push({
                    x0: x - 3,
                    y0: args.textPad - 3,
                    x1: x + square + args.textPad/2 + context.measureText(args.legend[i]).width + 3,
                    y1: args.textPad + args.textHeight + 2,
                    i: i
                });
                x += square + args.textPad/2;
                context.fillStyle = '#000';
                context.textAlign = 'left';
                context.textBaseline = 'top';
                context.fillText(
                    args.legend[i],
                    x,
                    args.textPad
                );
                x += context.measureText(args.legend[i]).width;
            }
        }
        context.fillStyle = 'rgba(255, 255, 255, 0.75)';
        clickRegions.forEach(function(region, i) {
            if (!args.toggleData[i]) {
                context.fillRect(region.x0, region.y0, region.x1 - region.x0, region.y1 - region.y0);
            }
        });

        // Curva
        if (args.data.length > 0) {
            if (args.data[0].y instanceof Array) {
                var ref = minY;
                var dRef = dY;
                if (dY === 0) { // minY === maxY
                    ref = 0;
                    dRef = 2 * minY / h;
                }
                var sets = args.data[0].y.length
                for (var s = 0; s < sets; s++) {
                    if (doubleAxis && s === 1) {
                        if (dY2 > 0) {
                            ref = minY2;
                            dRef = dY2;
                        } else {
                            ref = 0;
                            dRef = 2 * minY2 / h;
                        }
                    }
                    if (args.toggleData[s]) {
                        if (args.data.length === 1) {
                            context.fillStyle = args.lineColor[s];
                            context.beginPath();
                            context.arc(
                                pl + w/2,
                                h + pt - ((args.data[0].y[s] - ref)/dRef),
                                1, 0, 2*Math.PI
                            );
                            context.fill();
                        } else {
                            context.strokeStyle = args.lineColor[s];
                            context.beginPath();
                            // 0
                            context.moveTo(pl + ((args.data[0].x - minX)/dX), h + pt - ((args.data[0].y[s] - ref)/dRef));
                            // 1+
                            for (var i = 1; i < args.data.length; i++) {
                                if (args.linkGaps !== true) {
                                    if (args.data[i].x - args.data[i-1].x > args.gapWidth) {
                                        context.stroke();
                                        context.beginPath();
                                        context.moveTo(pl + ((args.data[i].x - minX)/dX), h + pt - ((args.data[i].y - ref)/dRef));
                                        continue;
                                    }
                                }
                                context.lineTo(pl + ((args.data[i].x - minX)/dX), h + pt - ((args.data[i].y[s] - ref)/dRef));
                            }
                            context.stroke();
                        }
                    }
                }
            } else {
                var ref = minY;
                var dRef = dY;
                if (dY === 0) { // minY === maxY
                    ref = 0;
                    dRef = 2 * minY / h;
                }
                if (args.toggleData[0]) {
                    context.strokeStyle = args.lineColor;
                    context.beginPath();
                    // 0
                    context.moveTo(pl + ((args.data[0].x - minX)/dX), h + pt - ((args.data[0].y - ref)/dRef));
                    // 1+
                    for (var i = 1; i < args.data.length; i++) {
                        if (args.linkGaps !== true) {
                            if (args.data[i].x - args.data[i-1].x > args.gapWidth) {
                                context.stroke();
                                context.beginPath();
                                context.moveTo(pl + ((args.data[i].x - minX)/dX), h + pt - ((args.data[i].y - ref)/dRef));
                                continue;
                            }
                        }
                        context.lineTo(pl + ((args.data[i].x - minX)/dX), h + pt - ((args.data[i].y - ref)/dRef));
                    }
                    context.stroke();
                }
            }
        }

        //Eixo X
        context.strokeStyle = '#000';
        if (minY === 0 && dY > 0) {
            context.beginPath();
            context.moveTo(pl, h + pt);
            context.lineTo(w + pl, h + pt);
            context.stroke();
        }
        if (args.xAxisLabel) {
            context.textAlign = 'center';
            context.textBaseline = 'top';
            context.fillText(
                args.xAxisLabel,
                pl + w/2,
                pt + h + 2*args.textPad + args.textHeight
            );
        }

        // Eixo Y
        context.beginPath();
        context.moveTo(pl, pt);
        context.lineTo(pl, h + pt);
        context.stroke();
        if (doubleAxis) {
            context.beginPath();
            context.moveTo(w + pl, pt);
            context.lineTo(w + pl, h + pt);
            context.stroke();
        }
        if (args.yAxisLabel) {
            context.fillStyle = '#000';
            context.textAlign = 'center';
            context.textBaseline = 'top';
            context.save();
            context.translate(args.textPad, pt + h/2);
            context.rotate(-Math.PI/2);
            if (singleAxis) {
                context.fillText(args.yAxisLabel, 0, 0);
            }
            if (doubleAxis) {
                context.fillText(args.yAxisLabel[0], 0, 0);
            }
            context.restore();
            if (doubleAxis) {
                context.save();
                context.translate(pl + w + pr - 2*args.textPad, pt + h/2);
                context.rotate(-Math.PI/2);
                context.fillText(args.yAxisLabel[1], 0, 0);
                context.restore();
            }
        }

        context.restore(); // recuperando o primeiro .save()
    }
    canvas.addEventListener('click', function(event) {
        var x = event.pageX - canvas.getBoundingClientRect().x,
        y = event.pageY - canvas.getBoundingClientRect().y;
        clickRegions.forEach(function(region) {
            if (x > region.x0 && x < region.x1 && y > region.y0 && y < region.y1) {
                args.toggleData[region.i] = !args.toggleData[region.i];
                draw();
            }
        });
    });
    draw();
}
