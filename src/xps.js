var gXPS_tooltip = document.querySelectorAll('.tooltip');
var gXPS_boxTool = document.querySelectorAll('.boxTooltip');
var gXps_sysUrTy = 0;


window.onmousemove = function(e)
{
    var x = e.clientX
    var y = e.clientY
    gXPS_boxTool.forEach((el, index) => {
        gXPS_tooltip[index].style.top = y + 10 + 'px'
        gXPS_tooltip[index].style.left = x + 10 + 'px'
    })
}
//-------------------------------------------------------------------------------------------------

getElid('sidebarCollapse').addEventListener('click', function () {
    getElid('sidebar').classList.toggle('active');
    getElid('topnav-container').classList.toggle('topnav-open');
    getElid('topnav-container').classList.toggle('topnav-close');
    var pageContainers = document.querySelectorAll('.page-container');
    for(var i = 0; i < pageContainers.length; i++){
        pageContainers[i].classList.toggle('closed');
    }
});

var gCurrentTime        = new Date;
var gLastSecond         = 100;
var gCurrentPage        = 0;
var gRequestCountDown   = 1;
var gClockRequest       = 3;
var gNoConectionFlag    = 0;
var  gUserNameTable     = [];
var gDiscTableSelIdx    = 0;

setInterval(function (){
    var s = gCurrentTime.getSeconds();
    if(gLastSecond != s){
        gLastSecond = s;
          runSecondFunctions();
    }
    delete  gCurrentTime;
    gCurrentTime = new Date;
},100);
//-------------------------------------------------------------------------------------------------

function runSecondFunctions(){

    if(gClockRequest){
        gClockRequest -= 1;
    }else{
        gClockRequest = 3;
        myHttpGet('localTime.txt',localTimeCallback);
        return;
    }

    if(gConectionTimeout){
        if(gNoConectionFlag){
            gNoConectionFlag = 0;
            getElid('alert-offline').classList.toggle('show');
            setTimeout(function () {
                getElid('alert-offline').classList.toggle('overlay');
                getElid('alert-offline').classList.toggle('hide');
            }, 300);

            pLog("Comunicacao OK");
        }
        gConectionTimeout -= 1;
    }
    else{
        if(gNoConectionFlag == 0){
            gNoConectionFlag = 1;
            getElid('alert-offline').classList.toggle('overlay');
            getElid('alert-offline').classList.toggle('hide');
            setTimeout(function () {
                getElid('alert-offline').classList.toggle('show');
            }, 5);
            pLog("Sem comunicacao");
        }
    }

    if(gRequestCountDown){
        gRequestCountDown -= 1;
        return;
    }



    switch(gCurrentPage){
    case 0:
        getMonitCurrentValues();
        gRequestCountDown = 1;
        break;
    case 1:
        myHttpGet('hist.txt',histCallback);
        gRequestCountDown = 30;
        break;
    case 2:
        gRequestCountDown = 1000;
        break;
    case 3:
        infraGetLiveValues();
        gRequestCountDown = 1;
        break;
    case 4:
        config_read();
        gRequestCountDown = 1800; // 30 minutes
        break;
    case 5:
        gRequestCountDown = 1800; // 30 minutes
        break;
    case 6:
        liBat_readParams();
        gRequestCountDown = 5;        
        break;
    case 7:
        symmetry_getTensions();
        gRequestCountDown = 5;
        break;
    case 9:
        capTest_monit();
        gRequestCountDown = 1;
        break;
    }
}
//-------------------------------------------------------------------------------------------------

function infraParamCallback(text)
{
    pLog("Infra Param OK");
    pLog(text);
    fillInfraScreen(arraySpliter(text));
}
//-------------------------------------------------------------------------------------------------

function localTimeCallback(text)
{
    gClockRequest = 10;
    fillLocalClock(text);
}
//-------------------------------------------------------------------------------------------------

window.onload = function(){
    var inputs = document.querySelectorAll(".form-control");
    for(var i = 0; i < inputs.length; i++){
        inputs[i].setAttribute("data-olddata","");
    }
    myHttpGet('localTime.txt',localTimeCallback);
    sleepMs(150);
    getMonitCurrentValues();
    sleepMs(150);
    fInfraReadButton();
    try {
        username.innerHTML = sessionStorage.getItem('uSeR');
    }
    catch(err) {
        alert("Falha na janela");
    }
};
//-------------------------------------------------------------------------------------------------

function removeAllClassList()
{
    getElid('li-dash').classList.remove('active');
    getElid('li-hist').classList.remove('active');
    getElid('li-alar').classList.remove('active');
    getElid('li-infr').classList.remove('active');
    getElid('li-conf').classList.remove('active');
    getElid('li-netw').classList.remove('active');
    getElid('li-battery').classList.remove('active');
    getElid('li-simet').classList.remove('active');
    getElid('li-usuario').classList.remove('active');
    getElid('li-capacity').classList.remove('active');

    getElid('dash').classList.remove('active');
    getElid('hist').classList.remove('active');
    getElid('alar').classList.remove('active');
    getElid('infr').classList.remove('active');
    getElid('conf').classList.remove('active');
    getElid('netw').classList.remove('active');
    getElid('batt').classList.remove('active');
    getElid('simet').classList.remove('active');
    getElid('usuario').classList.remove('active');
    getElid('capacidade').classList.remove('active');

    getElid('cont-dash').classList.remove('active');
    getElid('cont-hist').classList.remove('active');
    getElid('cont-alar').classList.remove('active');
    getElid('cont-infr').classList.remove('active');
    getElid('cont-conf').classList.remove('active');
    getElid('cont-netw').classList.remove('active');
    getElid('cont-batt').classList.remove('active');
    getElid('cont-simet').classList.remove('active');
    getElid('cont-usuario').classList.remove('active');
    getElid('cont-capacity').classList.remove('active');

};

/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------       DASHBOARD    -------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
function enterMonitScreen()
{
    removeAllClassList();

    getElid('li-dash').classList.add('active');
    getElid('dash').classList.add('active');
    getElid('cont-dash').classList.add('active');

    // Set active page
    gCurrentPage = 0;
    getMonitCurrentValues();
    gRequestCountDown = 2;
};

getElid('dashboard').addEventListener('click', enterMonitScreen);


/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------       HISTORY      -------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
function enterHistScreen()
{
    removeAllClassList();

    getElid('li-hist').classList.add('active');
    getElid('hist').classList.add('active');
    getElid('cont-hist').classList.add('active');

    // Set active page
    gCurrentPage = 1;
    clearHistScreen();
    myHttpGet('hist.txt',histCallback);
    gRequestCountDown = 30;
};
//-------------------------------------------------------------------------------------------------

getElid('history').addEventListener('click', enterHistScreen);

/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*-------------------------------------       ALARM       ------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
getElid('alarms').addEventListener('click', function () {

    removeAllClassList();
    getElid('li-alar').classList.add('active');
    getElid('alar').classList.add('active');
    getElid('cont-alar').classList.add('active');

    getElid("alarmsave").addEventListener("click",alarmSaveButton);
    getElid("alarmread").addEventListener("click",alarmReadButton);

    // Set active page
    gCurrentPage = 2;
    // Fill screen
    alarmReadButton();
    gRequestCountDown = 1;
});

/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------    INFRASTRUCTURE   ------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
function enterInfracreen()
{
    removeAllClassList();
    getElid('li-infr').classList.add('active');
    getElid('infr').classList.add('active');
    getElid('cont-infr').classList.add('active');

    // Set active page
    gCurrentPage = 3;
    // Load screen
    fInfraReadButton();
    gRequestCountDown = 1;
};
//-------------------------------------------------------------------------------------------------

getElid('infra').addEventListener('click', enterInfracreen);



/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*-----------------------------------    CONFIGURATION    ------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------*/
function enterConfigcreen()
{
    removeAllClassList();
    getElid('li-conf').classList.add('active');
    getElid('conf').classList.add('active');
    getElid('cont-conf').classList.add('active');

    config_setSysType(gXps_sysUrTy);
    // Set active page
    gCurrentPage = 4;
    // Fill screen
    config_read();
    gRequestCountDown = 1800; // 30 minutes
};
//-------------------------------------------------------------------------------------------------

getElid('config').addEventListener('click', enterConfigcreen);

/*-------------------------------------------------------*/
/*-----------     NETWORK       -------------------------*/
/*-------------------------------------------------------*/
getElid('network').addEventListener('click', function () {

    removeAllClassList();
    getElid('li-netw').classList.add('active');
    getElid('netw').classList.add('active');
    getElid('cont-netw').classList.add('active');

    getElid("redesave").addEventListener("click", btSaveNet);
    getElid("rederead").addEventListener("click", btReadNet);

    // Set active page
    gCurrentPage = 5;
    // Fill screen
    btReadNet();
    gRequestCountDown = 2;
});

/*-------------------------------------------------------*/
/*-----------     Litium        -------------------------*/
/*-------------------------------------------------------*/
getElid('battery').addEventListener('click', function () {

    removeAllClassList();
    getElid('li-battery').classList.add('active');
    getElid('batt').classList.add('active');
    getElid('cont-batt').classList.add('active');

    // Set active page
    gCurrentPage = 6;
    // Fill Screen
    liBat_readParams();
    gRequestCountDown = 5;    
});

/*------------------------------------------------------*/
/*-----------------     SIMETRIA       -----------------*/
/*------------------------------------------------------*/
getElid('symmetry').addEventListener('click', function () {

    removeAllClassList();
    getElid('li-simet').classList.add('active');
    getElid('simet').classList.add('active');
    getElid('cont-simet').classList.add('active');

    // Set active page
    gCurrentPage = 7;
    // Fill screen
    symmetry_getAll('symett.txt',symmetry_getAllCallback);
    gRequestCountDown = 20;
});

/*-----------------------------------------------------*/
/*-----------------     USUARIO       -----------------*/
/*-----------------------------------------------------*/
getElid('user').addEventListener('click', function () {

    removeAllClassList();
    getElid('li-usuario').classList.add('active');
    getElid('usuario').classList.add('active');
    getElid('cont-usuario').classList.add('active');

    // Set active page
    gCurrentPage = 8;
    usersGetTable();
    gRequestCountDown = 2;
});

/*---------------------------------------------------------*/
/*---------------- TESTE DE CAPACIDADE --------------------*/
/*---------------------------------------------------------*/
getElid('capacity').addEventListener('click', function () {
    removeAllClassList();
    getElid('li-capacity').classList.add('active');
    getElid('capacidade').classList.add('active');
    getElid('cont-capacity').classList.add('active');

    // Set active page
    gCurrentPage = 9;
    refreshCharts();
    capTest_paramBtRead();
    capTest_monit();
    gRequestCountDown = 2;
});



// Modals
var capTablesModal = getElid("captestTables_modal");
var capTablesTrigger = getElid("captestTables_trigger");
var capTablescloseButton = getElid("captestTables_cancel");

var capResultModal = getElid("captestResults_modal");
var capResultscloseButton = getElid("captestResults_cancel");

var capCurrentModal = getElid("captestCurrent_modal");

// ESC Detection
var escEventHandler;
function attachEscEvent(cb) {
    escEventHandler = function (evt) {
        console.log('esc');
        evt = evt || window.event;
        var isEscape = false;
        if ("key" in evt) {
            isEscape = (evt.key === "Escape" || evt.key === "Esc");
        } else {
            isEscape = (evt.keyCode === 27);
        }
        if (isEscape) {
            cb();
        }
    }
    document.addEventListener('keydown', escEventHandler);
}
function dettachEscEvent() {
    document.removeEventListener('keydown', escEventHandler);
}
// ESC Detection

function capTablesShowModal() {
    capTablesModal.classList.add("show-modal");
    attachEscEvent(capTablesHideModal);
}
function capTablesHideModal() {
    capTablesModal.classList.remove("show-modal");
    dettachEscEvent();
}
function capCurrentShowModal() {
    capCurrentModal.classList.add("show-modal");
    attachEscEvent(capCurrentHideModal);
}
function capCurrentHideModal() {
    capCurrentModal.classList.remove("show-modal");
    dettachEscEvent();
}
function capResultsShowModal() {
    capResultModal.classList.add("show-modal");
    attachEscEvent(capResultsHideModal);
}
function capResultsHideModal() {
    capResultModal.classList.remove("show-modal");
    dettachEscEvent();
}

capTablesTrigger.addEventListener("click", capTablesShowModal);
capTablescloseButton.addEventListener("click", capTablesHideModal);

capResultscloseButton.addEventListener("click", capResultsHideModal);

function tabClick (event, ctxId, itemCls, paneCls, i) {
    event.preventDefault();
    if (event.target.parentElement.classList.contains('active')) {
        return;
    }
    var tabItens = document.querySelectorAll('#' + ctxId + ' .' + itemCls + '.active');
    var tabPanes = document.querySelectorAll('#' + ctxId + ' .' + paneCls + '.active');
    tabItens[0].classList.remove('active');
    tabPanes[0].classList.remove('active');
    event.target.parentElement.classList.add('active');
    var tabId = event.target.attributes.href.value.replace('#', '');
    getElid(tabId).classList.add('active');
    gDiscTableSelIdx = i;
}

// Toggle 'Gráficos/Tabela'
getElid('captestResult_showCharts').addEventListener('click', function () {
    this.classList.add('active');
    getElid('captestResult_charts').classList.add('active');
    getElid('captestResult_showTable').classList.remove('active');
    getElid('captestResult_table').classList.remove('active');
});
getElid('captestResult_showTable').addEventListener('click', function () {
    this.classList.add('active');
    getElid('captestResult_table').classList.add('active');
    getElid('captestResult_charts').classList.remove('active');
    getElid('captestResult_showCharts').classList.remove('active');
});

// Dimensões dos gráficos
function refreshCharts() {
    setTimeout(function () {
        var h = getElid('captestResult_charts').getBoundingClientRect().height/2;
        var w = getElid('captestResult_charts').getBoundingClientRect().width;
        pow = getElid('captestResult_power');
        pow.setAttribute('width', w);
        pow.setAttribute('height', h);
        tem = getElid('captestResult_temp');
        tem.setAttribute('width', w);
        tem.setAttribute('height', h);
    }, 100);
}

// Habilitar formulário
function habilitarCapTest (evt) {
    var checked = evt.target.checked;
}

/*-----------------------------------*/
/*-----         GERAL           -----*/
/*-----------------------------------*/
///System clock
var gTt = 0;
function refreshTime(mls){
    var tms = "";
    var time = parseInt(mls,16) + gTt;
    var tm = formatUtcMiliSec(time*1000);
    tm = tm.split(" - ");
    getElid("hm").innerHTML = tm[1];
    getElid("dma").innerHTML = tm[0];
    getElid("nethmsdma").innerHTML = tm[0] + " - " + tm[1];
    tms = time.toString(16);
    if(time.toString().length < 8){
        tms = zero(8-tms.toString().length) + tms;
    }
    getElid("nethmsdma").setAttribute("data-timeutc",tms);
    getElid("hm").setAttribute("data-timeutc",tms);
    gTt = (gTt<60)?gTt+1:0;0;
}

var clockReqTimer;

function fillLocalClock(stringInput){
    var rsp = stringInput.split("!");
    getElid("xpsnumber").innerHTML = rsp[0];
    gTt = 0;
    clearInterval(clockReqTimer);
    clockReqTimer = setInterval(refreshTime,1000,rsp[1]);
}


function ipMaskShow(ip){
    ip = ip.split(".");
    for(var i = 0; i < ip.length; i++){
        ip[i] = parseInt(ip[i]);
    }
    return ip[0].toString().concat(".",ip[1].toString(),".",ip[2].toString(),".",ip[3].toString());
}

function ipMaskSend(ip){
    if(ip != null){
        ip = ip.split(".");
        for(var i = 0; i < ip.length; i++){
            if(ip[i].length == 1){
                ip[i] = zero(2) + ip[i].toString();
            }
            else if(ip[i].length == 2){
                ip[i] = zero(1) + ip[i].toString();
            }
            ip[i] = ip[i].toString();
        }
        return ip[0].concat(".",ip[1],".",ip[2],".",ip[3]);
    }
    else{
        return "000.000.000.000";
    }
}

var onlyS = RegExp("[^[\\sa-zA-Z0-9]]*","g");
function onlyString(event){
    if(onlyS.test(event.target.value) == true){
        event.target.value = event.target.value.replace(onlyS,'');
    }
    else{
        event.target.setAttribute('value', event.target.value);
    }
}

var rCommunityStr = RegExp("[^\-\_\~\.a-zA-Z0-9]*","g");
function regexCommunityString(event){
    if(rCommunityStr.test(event.target.value) == true){
        event.target.value = event.target.value.replace(rCommunityStr,'');
    }
    else{
        event.target.setAttribute('value', event.target.value);
    }
}

var onlySnS = RegExp("[^[a-zA-Z0-9]]*","g");
function onlyStringNoSp(event){
    if(onlySnS.test(event.target.value) == true){
        event.target.value = event.target.value.replace(onlySnS,'');
    }
    else{
        event.target.setAttribute('value', event.target.value);
    }
}

var onlyN = RegExp("[^-[.,0-9]]*","g");
function onlyNumber(event){
    if(onlyN.test(event.target.value) == true){
        event.target.value = event.target.value.replace(onlyN,'');
    }
    else{
        event.target.setAttribute('value', event.target.value);
    }
}

var onlyNR = RegExp("[^-[0-9]]*","g");
function onlyNumberReally(event){
    if(onlyNR.test(event.target.value) == true){
        event.target.value = event.target.value.replace(onlyNR,'');
    }
    else{
        event.target.setAttribute('value', event.target.value);
    }
}

var onlyI = RegExp("[^[.0-9]]*","g");
function onlyIP(event){
    if(onlyI.test(event.target.value) == true){
        event.target.value = event.target.value.replace(onlyI,'');
    }
    else{
        event.target.setAttribute('value', event.target.value);
    }
}

var onlyIPv6regex = RegExp("[^[:0-9a-fA-F]]*","g");
function onlyIPv6(event){
    if(onlyIPv6regex.test(event.target.value) == true){
        event.target.value = event.target.value.replace(onlyIPv6regex,'');
    }
    else{
        event.target.setAttribute('value', event.target.value);
    }
}

var noSp = RegExp("[\s]*","g");
function noSpace(event){
    if(noSp.test(event.target.value) == true){
        event.target.value = event.target.value.replace(noSp,'');
    }
    else{
        event.target.setAttribute('value', event.target.value);
    }
}

var onlyEm = RegExp("[^[.@a-zA-Z0-9]]*","g");
function onlyEmail(event){
    if(onlyEm.test(event.target.value) == true){
        event.target.value = event.target.value.replace(onlyEm,'');
    }
    else{
        event.target.value = event.target.value.replace(event.target.value,event.target.value.toLowerCase());
        event.target.setAttribute('value', event.target.value.toLowerCase());
    }
}

function lengthDet(event, len){
    if(event.target.value.length > len){
        event.target.value = event.target.value.replace(event.target.value[len],'');
    }
    else{
        event.target.setAttribute('value', event.target.value);
    }
}

function ipMaskCheck(event){
    var ip = event.target.value.split(".");
    var c = 0;
    for(var i = 0; i < ip.length; i++){
        if(ip[i] == ""){
            c = 1;
        }
    }
    if(ip.length != 4 || c == 1){
        c = 0;
        event.target.classList.add('inputErrorBorder');
    }
    else{
        event.target.setAttribute('value', ipMaskShow(event.target.value));
        event.target.value = ipMaskShow(event.target.value);
        event.target.classList.remove('inputErrorBorder');
    }
}

function ipv6MaskCheck(event){
    var ip = event.target.value.split(":");
    if(ip.length !== 8){
        event.target.classList.add('inputErrorBorder');
    }
    else{
        event.target.classList.remove('inputErrorBorder');
    }
}

function rangeDet(event, min, max){
    if(event.target.value < min || event.target.value > max){
        event.target.classList.add('inputErrorBorder');
    }
    else{
        event.target.classList.remove('inputErrorBorder');
    }
}

function addGrand(event, G){
    /*var grand = new RegExp(G);
    if(event.target.value != "" && grand.test(event.target.value) == false){
        event.target.value = event.target.value + " " + G;
        event.target.setAttribute('value', event.target.value + " " + G);
    }*/
}

var inputChange = document.querySelectorAll('input');

for(var c = 0; c < inputChange.length; c++){
    inputChange[c].addEventListener('keyup', function(event){
        if(event.target.value == event.target.dataset.olddata){
            event.target.classList.remove('inputChangeBorder');
        }
        else{
        }
    });
}
for(var c = 0; c < inputChange.length; c++){
    inputChange[c].addEventListener('focusout', function(event){
        if(event.target.value == event.target.dataset.olddata){
            event.target.classList.remove('inputChangeBorder');
        }
        else{
            event.target.classList.add('inputChangeBorder');
        }
    });
}

function simbol(n) {
    var ss = "";
    for (var i = 0; i < n; i++){
        ss += "?";
    }
    return ss;
}

function zero(n) {
    var ss = "";
    for (var i = 0; i < n; i++){
        ss += "0";
    }
    return ss;
}

function sleepMs(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}


function formatUtcMiliSec(milsecs) {
    var eD;
    var t = new Date(milsecs);
    t.setTime(t.getTime() + (t.getTimezoneOffset()*1000*60));
	y = t.getFullYear();
	m  = t.getMonth()+1;
	d  = t.getDate();
	h  = t.getHours();
	mi = t.getMinutes();
	s  = t.getSeconds();
	eD = "";
	if(d < 10){
		eD += "0";
	}
	eD += d + "/";
	if(m<10){
		eD += "0";
	}
	eD += m + "/" +y;
	eD += " - ";
	if(h < 10){
		eD += "0";
	}
	eD += h + ":";
	if(mi<10){
		eD += "0";
	}
	eD += mi + ":";
	if(s<10){
		eD += "0";
	}
	eD += s;
	return eD;
};

function arraySpliter(stringInput)
{
    var paineis = stringInput.split("!");
    var arrayX = [];
    var painel = [];
    var elementos = [];
    var x = 0;
    for(var i = 0; i<paineis.length;i++)
    {
        painel = paineis[i].split('*')
        for(var j=0;j<painel.length;j++)
        {
            elementos = painel[j].split(':');
            for(var k=0;k<elementos.length;k++)
            {
                arrayX[x] = elementos[k];
                x++;
            }
        }
    }
    return arrayX;
};

function xpsEnable(v)
{
    for(var i=0; i<v.length;i++){
        v[i].removeAttribute("disabled");
        v[i].classList.remove("disabled");
    }
};
//-------------------------------------------------------------------------------------------------

function xpsDisable(v)
{
    for(var i=0; i<v.length;i++){
        v[i].setAttribute("disabled",true);
        v[i].classList.add("disabled");
    }
};
//-------------------------------------------------------------------------------------------------

var xpsIpRegex = /^((\d{1,2}|1\d{2}|2[0-4]\d|25[0-5])\.){3}(\d{1,2}|1\d{2}|2[0-4]\d|25[0-5])$/;
var xpsPortRegex = /^[0-9]{1,5}$/;


function local_verifyCharacter(s)
{
    var e = s.indexOf('?');
    if(e >= 0){
        xpsAlert("Caracter '?' não é aceito.");
        return 1;
    }

    e = s.indexOf('#');
    if(e >= 0){
        xpsAlert("Caracter '?' não é aceito.");
        return 1;
    }

    return 0;
}
//---------------------------------------------------------------------------------------------

function local_verifyPassword(p1,p2)
{
    if(p1 != p2){
        xpsAlert("Senhas diferentes");
        return 1;
    }

    if(p1.length <4){
        xpsAlert("Mínimo de 4 caracteres");
        return 1;
    }

    return local_verifyCharacter(p1);
}
//-------------------------------------------------------------------------------------------------

function usersGetTable()
{
    myHttpGet('geTUsEr.txt',usersGetTableCallBack);
}
//-------------------------------------------------------------------------------------------------

function usersGetTableCallBack(text)
{
    users = text.split(",");

    if(users[users.length-1] != '@_@'){
        xpsAlert("Formato errado:" + text);
        return;
    }

    gUserNameTable = [];

    for(tb ='',i=0; i<users.length-1; i++){
        lines = users[i].split(":");
        name = lines[0];
        gUserNameTable.push(name);
        priv = lines[1]=='3'?'Administrador':'Operador';
        tb+= '<tr>';
        tb+= '<td>' + name + '</td>';
        tb+= '<td>' + priv +'</td>';
        tb+= '<td><button type="button" class="btn btn-danger delete"  onclick="userDeleteUser(' +i+ ')">Excluir</button></td>';
        tb+= '</tr>';
    }

    userTable.innerHTML = tb;
};
//-------------------------------------------------------------------------------------------------

function userNewUser()
{
    var p1 = inputNewPass1.value;
    var p2 = inputNewPass2.value;
    var tt = local_verifyPassword(p1,p2);
    if(tt){
        return;
    }

    var s = inputNewName.value;
    if(local_verifyCharacter(s)){
        return;
    }

    s += '?'+ p1.hashCode() + '?'
    s += cbUserPriv.selectedIndex==0?'3?':'1?';
    myHttpPost("NeWUseR.txt?"+s, userNewUserCallBack);
    pLog(s);
}
//-------------------------------------------------------------------------------------------------

function userNewUserCallBack(text)
{
    switch(text){
    case "OK":
        usersGetTable();
        xpsAlert("Usuário adicionado!");
        break;

    case "EXIST":
        xpsAlert("Usuário com este nome já existe.");
        break;

    case "MAXNM":
        xpsAlert("Número máximo de usuarios alcançado.");
        break;

    case "FORMT":
        xpsAlert("Erro no formato da string.");
        break;

    default:
        xpsAlert("Erro desconhecido ->" + text + "<-");
    }

    inputNewName.value  = "";
    inputNewPass1.value = "";
    inputNewPass2.value = "";
}
//-------------------------------------------------------------------------------------------------

function userChangePass()
{
    var ps = sessionStorage.getItem('pasSwD');
    var pn = senhaAtual.value.hashCode();

    if(ps != pn){
        xpsAlert("Senha atual errada.");
        return;
    }

    var p1 = inputChangePass1.value;
    var p2 = inputChangePass2.value;
    var tt = local_verifyPassword(p1,p2);
    if(tt){
        return;
    }

    var s = changepassus.innerHTML +'?'+ p1.hashCode()+'?';
    myHttpPost("NeWPaSs.txt?"+s, userChangePassCallBack);
};
//-------------------------------------------------------------------------------------------------

function userChangePassCallBack(text)
{
    if(text != 'OK'){
        xpsAlert("Erro na troca de senha.");
        return;
    }

    p = inputChangePass1.value.hashCode();
    sessionStorage.setItem('pasSwD', p);
    xpsAlert("Senha alterada com sucesso.");

    inputNewName.value = "";
    inputNewPass1.value = "";
    inputNewPass2.value = "";

    // Close the window.
    toggleModalChangePass();
};
//-------------------------------------------------------------------------------------------------

function userDeleteUser(idx)
{
    n = gUserNameTable[idx];
    c = sessionStorage.getItem('uSeR');
    if(n == c){
        xpsAlert("Não é possível excluir próprio usuário.", "Excluir usuário:");
        return;
    }
    gUserNameTable.splice(idx,1);
    myHttpPost("delEtusEr.txt?"+idx,userDeleteUserCallBack);
};
//-------------------------------------------------------------------------------------------------

function userDeleteUserCallBack(text)
{
    switch(text){
    case "OK":
        usersGetTable();
        break;
    case "LSADM":
        xpsAlert("Não foi possível excluir usuário. É necessário pelo menos 1 administrador")
        break;
    }
};
//-------------------------------------------------------------------------------------------------

function existSystemNow(event)
{
    sessionStorage.clear();
};
//-------------------------------------------------------------------------------------------------

function toggleModalChangePass()
{
    u = sessionStorage.getItem('uSeR');
    if( u == 'root'){
        xpsAlert("Não é possível trocar a senha do super-usuário");
        return;
    }

    a = document.querySelector(".modal_login");
    a.classList.toggle("show-modal");
    changepassus.innerHTML = u;
};
//-------------------------------------------------------------------------------------------------

var Vmodal = document.querySelector(".mod");
var closeButton = document.querySelectorAll(".OK");
function viewModal() {
    Vmodal.classList.toggle("show-modal");
};
//-------------------------------------------------------------------------------------------------

closeButton[0].addEventListener("click", viewModal);
// window.addEventListener("click", windowOnClick);


/* Alerts */

function xpsAlert(text, title) {
    var alertArea = getElid('alert-area');
    var alertHtml = '';
    alertHtml += '<div class="alert-modal">';
    if (title !== undefined && title.length > 0) {
        alertHtml += '    <div class="alert-modal-header">'+title+'</div>';
    }
    alertHtml += '    <div class="alert-modal-body">'+text+'</div>';
    alertHtml += '    <div class="alert-modal-controls">';
    alertHtml += '        <button class="btn btn-primary btn-xps" onclick="xpsAlertClose()">OK</button>';
    alertHtml += '    </div>';
    alertHtml += '</div>';
    alertArea.innerHTML = alertHtml;
    alertArea.classList.remove('hide');
    alertArea.classList.add('overlay');
    setTimeout(function () {
        alertArea.classList.add('show');
    }, 5);
};
//-------------------------------------------------------------------------------------------------

function xpsAlertClose() {
    var alertArea = getElid('alert-area');
    alertArea.classList.remove('show');
    setTimeout(function () {
        alertArea.classList.remove('overlay');
        alertArea.innerHTML = '';
        alertArea.classList.add('hide');
    }, 300);
};
//-------------------------------------------------------------------------------------------------

function getDecimalSeparator() {
    //fallback
    var decSep = ".";
    try {
        // this works in FF, Chrome, IE, Safari and Opera
        var sep = parseFloat(3/2).toLocaleString().substring(1,2);
        if (sep === '.' || sep === ',') {
            decSep = sep;
        }
    } catch(e){}
    return decSep;
};
//-------------------------------------------------------------------------------------------------
