var gcapTestParams = [
getElid("cParamEnab"),
getElid("cParamAuto"),
getElid("cParamPeri"),
getElid("cParamType"),
getElid("cParamFirs"),
getElid("cParamInte"),
getElid("cParamVfin"),
getElid("cParamDmax"),
getElid("cParamTmax"),
getElid("cParamResv"),
getElid("cParamTabl"),
];

var gCapTestHistTable = getElid("captestHistory");

var gCapTestResult = [
getElid("cTstResu_name"),
getElid("cTstResu_resu"),
getElid("cTstResu_ini"),
getElid("cTstResu_end"),
getElid("cTstResu_npts"),
getElid("cTstResu_vEnd"),
getElid("cTstResu_txds"),
getElid("cTstResu_smrt"),
getElid("cTstResu_capa"),
getElid("cTstResu_imed"),
getElid("cTstResu_tmed"),
getElid("cTstResu_dur"),
getElid("cTstResu_tbdc")
];

var gCapTestDiscTables = [
getElid("captestDischargeTable1"),
getElid("captestDischargeTable2"),
getElid("captestDischargeTable3"),
getElid("captestDischargeTable4"),
getElid("captestDischargeTable5")
];

var gCapTestDiscTableNames = [
getElid("captestModelName1"),
getElid("captestModelName2"),
getElid("captestModelName3"),
getElid("captestModelName4"),
getElid("captestModelName5"),
];


// CAPACITY_END_FAST_BY_USCC			0 // Cancel
// CAPACITY_END_FAST_BY_WEB			    1 // Cancel
// CAPACITY_END_BY_AC_RETURN			2 // Fail
// CAPACITY_END_FAST_BY_VMIN			3 // Fail
// CAPACITY_END_FAST_BY_TIME			4 // OK
// CAPACITY_END_FAST_BY_AH				5 // OK
// CAPACITY_END_FULL_TEST				6 // Need to do some math to tell FAIL or OK
// CAPACITY_END_FULL_BY_TIME			7 // Cancel
// CAPACITY_END_FULL_BY_USCC			8 // Cancel
// CAPACITY_END_FULL_BY_WEB			    9 // Cancel
// CAPACITY_END_BY_LOW_BUS_VOLTAGE		10 // Fail
// CAPACITY_END_DISABLE_CAP_TEST		11 // Cancel
// CAPACITY_END_BAD_COMPARE_TABLE		12 // Fail
// CAPACITY_END_FULL_LESS_80            13 // Fail

var gTestResultArray = [0, 0, 2, 2, 1, 1, 1, 0, 0, 0, 1, 0, 2, 2]; // 0=Cancel, 1=OK, 2=Fail

var gcapTest_history_time = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var gcapTest_history_resu = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
var gcapTest_history_indx = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];

var gcapTestParamsValues = [0,0,0,0,1565263993,7,480,300,60,1,0];
var gCapTest_dTableMin = [new Array(60).fill(0),new Array(60).fill(0),new Array(60).fill(0),new Array(60).fill(0),new Array(60).fill(0)];
var gCapTest_dTableAH  = [new Array(60).fill(0),new Array(60).fill(0),new Array(60).fill(0),new Array(60).fill(0),new Array(60).fill(0)];
var gResultDataHeader = "";
var gResultDataArray = [];
var gTestRunningFlag = 0;

function local_formatTimeFromSec(n)
{
    var s = '';
    var p;
    var v;

    // Get the days
    v = 86400;
    if(n >=v){
        p = Math.floor(n/v);
        n %= v;
        s += p.toString() + 'D, ';
    }

    // Get the hours
    v = 3600;
    p = Math.floor(n/v);
    n %= v;
    if(p<10){
        s+= '0';
    }
    s += p.toString() + ':';

    // Get the minutes
    v = 60;
    p = Math.floor(n/v);
    n %= v;
    if(p<10){
        s+= '0';
    }
    s += p.toString() + ':';

    // Get the seconds
    if(n<10){
        s+= '0';
    }
    s += n.toString();

    return s;
};
//-------------------------------------------------------------------------------------------------

function local_clearBorder(v)
{
    v.classList.remove('inputChangeBorder');
    v.classList.remove('inputErrorBorder');
};
//-------------------------------------------------------------------------------------------------

// function local_clearAllBorders()
// {
//     for(var i = 0; i < gcapTestParams.length; i++){
//         gcapTestParams[i].classList.remove('inputChangeBorder');
//         gcapTestParams[i].classList.remove('inputErrorBorder');
//     }
// };
// //-------------------------------------------------------------------------------------------------
function local_clearAllBorders()
{
    for(var i = 0; i < gcapTestParams.length; i++){
        local_clearBorder(gcapTestParams[i]);
    }
};
//-------------------------------------------------------------------------------------------------

//----------------------------
//---   Main Screen Data   ---
//----------------------------
function capTest_en()
{
    var a = gcapTestParams.slice(0);
    a.shift();
    if(gcapTestParams[0].checked){
        xpsEnable(a);
        capTest_fullEnabled();        
    }
    else{
        xpsDisable(a);
   }
};
//-------------------------------------------------------------------------------------------------

function capTest_fullEnabled()
{
    var v = [gcapTestParams[6],gcapTestParams[7]];
    var idx = gcapTestParams[3].selectedIndex;

    xpsDisable(v);
    if(idx == 0){
        xpsEnable(v);
    }
};
//-------------------------------------------------------------------------------------------------

function capTest_testValues(valuesVec)
{
    // Intervalo (dias) [1 a 255]
    // Tensao Final     [450 a 520]
    // Descarga maxima  [10 a 9999]
    // T max            [1 a 2880]
    // T reserva        [1 a 3600]

    if(isNaN(valuesVec[5])) return 1;
    if(isNaN(valuesVec[6])) return 2;
    if(isNaN(valuesVec[7])) return 3;
    if(isNaN(valuesVec[8])) return 4;
    if(isNaN(valuesVec[9])) return 5;

    if(valuesVec[5] < 1 || (valuesVec[5]) > 255){
        return 1;
    }

    if(valuesVec[6] < 450 || (valuesVec[6] > 520)){
        return 2;
    }

    if(valuesVec[7] < 10 || (valuesVec[7] > 9999)){
        return 3;
    }

    if(valuesVec[8] < 1 || (valuesVec[8] > 2880)){
        return 4;
    }

    if(valuesVec[9] < 1 || (valuesVec[9] > 3600)){
        return 5;
    }

    return 0;
};
//-------------------------------------------------------------------------------------------------

function capTest_readParamScreen()
{
    var z;
    var ret;
    var ds = (1/2).toString()[1]

    gcapTestParamsValues[0] = gcapTestParams[0].checked?1:0;
    gcapTestParamsValues[1] = gcapTestParams[1].checked?1:0;
    gcapTestParamsValues[2] = gcapTestParams[2].checked?1:0;
    gcapTestParamsValues[3] = gcapTestParams[3].selectedIndex;

    z = gcapTestParams[4].value.split('-');
    gcapTestParamsValues[4] = Date.UTC(z[0],z[1]-1,z[2])/1000;;

    gcapTestParamsValues[5] = gcapTestParams[5].value.replace(',','.') * 1;          // Intervalo (dias) [1 a 255]
    gcapTestParamsValues[6] = gcapTestParams[6].value.replace(',','.') * 10;         // Tensao Final     [450 a 520]
    gcapTestParamsValues[7] = gcapTestParams[7].value.replace(',','.') * 10;         // Descarga maxima  [10 a 9999]
    gcapTestParamsValues[8] = gcapTestParams[8].value.replace(',','.') * 1;          // T max            [1 a 2880]
    gcapTestParamsValues[9] = gcapTestParams[9].value.replace(',','.') * 1;          // T reserva        [1 a 3600]
    gcapTestParamsValues[10]= gcapTestParams[10].selectedIndex;

    ret = capTest_testValues(gcapTestParamsValues)
    if(ret){
        return ret;
    }

    return 0;
};
//-------------------------------------------------------------------------------------------------

function capTest_buildScreen()
{
    var dt; 

    local_clearAllBorders();
    gcapTestParams[0].checked = gcapTestParamsValues[0];
    gcapTestParams[1].checked = gcapTestParamsValues[1];
    gcapTestParams[2].checked = gcapTestParamsValues[2];
    gcapTestParams[3].selectedIndex = gcapTestParamsValues[3];

    dt = formatUtcMiliSec(gcapTestParamsValues[4]*1000).split('-')[0].split('/');
    gcapTestParams[4].value = dt[2].trim()+'-'+dt[1]+'-'+dt[0];
  
    gcapTestParams[5].value = gcapTestParamsValues[5];
    gcapTestParams[6].value = (gcapTestParamsValues[6]/10).toFixed(1);
    gcapTestParams[7].value = (gcapTestParamsValues[7]/10).toFixed(1);
    gcapTestParams[8].value = gcapTestParamsValues[8];
    gcapTestParams[9].value = gcapTestParamsValues[9];
    gcapTestParams[10].selectedIndex = gcapTestParamsValues[10];
};
//-------------------------------------------------------------------------------------------------

function capTest_buildIniTypeTable(d)
{
    var r = [" "," lbl-ok "];
    var s = ""
    
    v = [0,0,0,0];
    v[d] = 1;

    s  = '<h4>Iniciado</h4>'
    s += '<span class="lbl'+ r[v[0]] +'w-100">USCC</span>';
    s += '<span class="lbl'+ r[v[1]] +'w-100">WEB</span>';
    s += '<span class="lbl'+ r[v[2]] +'w-100">AUTOMÁTICO</span>';
    s += '<span class="lbl'+ r[v[3]] +'w-100">PERIÓDICO</span>';
    
    return s;
}
//-------------------------------------------------------------------------------------------------

function capTest_buildResultTypeTable(d)
{
    var r = [" "," lbl-ok "," lbl-fail "];
    var h = getElid("captestResult_endTable");
    var s = ""
    
    s += '<div class="col-xs-6"> <span class="lbl';
    s += r[d[0]];
    s += 'w-100">USCC</span> <span class="lbl';
    s += r[d[1]];
    s += 'w-100">WEB</span> <span class="lbl'
    s += r[d[2]];
    s += 'w-100">RETORNO CA</span> </div> <div class="col-xs-6"><span class="lbl'
    s += r[d[3]];
    s += 'w-100">TMP MAX</span> <span class="lbl'
    s += r[d[4]];
    s += 'w-100">Ah MAX</span> <span class="lbl'
    s += r[d[5]];
    s += 'w-100">V FINAL</span></div>'

    h.innerHTML = s;
}
//-------------------------------------------------------------------------------------------------


//-------------------------------------
//---   Read the history of tests   ---
//-------------------------------------
function capTest_loadHist(n)
{
    var s = "";
    var t = gcapTest_history_time; // Time of the test
    var r = gcapTest_history_resu; // Result of test
    var x = gcapTest_history_indx; // Index of test
    var gra = '<span class="lbl">CANCELADO</span>';
    var blu = '<span class="lbl lbl-ok">OK</span>';
    var yel = '<span class="lbl lbl-fail">FALHA</span>';
    var red = '<span class="lbl lbl-repr">REPROVADO</span>';

    for(var i=0; i<n; i++){
        if(t[i] == 0){
            break;
        }
        var dt = formatUtcMiliSec(t[i]*1000).split('-') 
        var d = dt[0];
        var h = dt[1];
        s += '<tr onclick="capTest_loadHistIndex('+x[i]+')"><td>'+ (i+1) + '</td><td>';
        s += d;
        s += '</td><td>';
        s += h;
        s += '</td><td>';
        s += r[i]==1?blu:r[i]==0?gra:yel;
        s += '</td><td><img class="charticon"></td></tr>';        
    }

    gCapTestHistTable.innerHTML = s;
};
//-------------------------------------------------------------------------------------------------

function capTest_paramHistoryRead()
{
    myHttpGet('rCapHiSt.txh',capTest_paramHistoryRead_callback);
};
//-------------------------------------------------------------------------------------------------

function capTest_paramHistoryRead_callback(text)
{
    var s = text.split("*");
    var t = gcapTest_history_time;
    var r = gcapTest_history_resu;
    var x = gcapTest_history_indx;
 
    pLog(text);

    if (s.length == 1){
        pLog(text);
        xpsAlert("Erro: Formato da resposta.(1)");
        return;
    }
    // Get the number of nodes.
    n = parseInt(s[0],16);
    if(n==0){
        capTest_loadHist(0);
        return;
    }

    // Get the nodes
    var ts  = s[1].split(";");
    if(ts.length != n){
        pLog(text);
        capTest_loadHist(0);
        xpsAlert("Erro: Formato da resposta.(2)");
        return;
    }

    for(var i=0; i<n;i++){
        var e = ts[i].split(",");
        t[i] = parseInt(e[0],16);
        r[i] = parseInt(e[1],16);
        x[i] = parseInt(e[2],16);

        if(r[i] >= gTestResultArray.length){
            pLog(text);
            capTest_loadHist(i);
            xpsAlert("Erro: Formato da resposta.(3)");
            return;
        }
        // Get the code for the especific result. 
        r[i] = gTestResultArray[r[i]];
    }
    capTest_loadHist(n);
};
//-------------------------------------------------------------------------------------------------


//--------------------------------------------------------------
//---   Get the result of a test clicked on history table    ---
//--------------------------------------------------------------


// Get the test data loaded on the memory of the uControler. 
// The test index was passed by the function 'capTest_loadHistIndex'
function capTest_getDataBuffer()
{
    myHttpGet('CapTestDataGet',capTest_getDataBuffer_callback);    
}
//-------------------------------------------------------------------------------------------------

function capTest_getDataBuffer_callback(t)
{
    var data = [];

    if(t.length == 2){
        xpsAlert("Loading");
        return;
    }
    
    // Split nodes 
    var n = t.split(';');
    // Remove the last node (the empty one)
    n.pop(); 
    // Build the nodes array 
    for(var i=0; i<n.length; i++){
        // Split values of the node
        var v = n[i].split(',');
        if(v.length != 6){
            xpsAlert("String mal formada.");
            return;
        }

        var d = [];
        for (var j =0; j<v.length; j++){
            var tt =  parseInt(v[j],16);
            if (tt > 0x80000000){
                tt = 0x100000000 - tt;
                tt*= -1;
            }
            if(j==1 || j==2){
                tt = (tt/10).toFixed(1);
            }
            d.push(tt);
        }
        // Put time in milisecons 
        d[0] *= 1000; 
        if(d[3]==-201){d[3] = 'Sem Sensor'}
        if(d[3]==-202){d[3] = 'Sensor Inv'}
        if(d[4]==-201){d[4] = 'Sem Sensor'}
        if(d[4]==-202){d[4] = 'Sensor Inv'}
        if(d[5]==-201){d[5] = 'Sem Sensor'}
        if(d[5]==-202){d[5] = 'Sensor Inv'}
        data.push(d)
    }
    gResultDataArray = data.slice(); // copy the data into 'gResultDataArray'
    getElid('captestResult_showCharts').click(); // primeira aba
    capTest_fillTable(data);
    capTest_drawCharts(data,((gSampleRateInSeconds*1000)+1000));
    // Show Screen. 
    capResultsShowModal();
}
//-------------------------------------------------------------------------------------------------

function capTest_loadHistIndex(idx)
{   
    if(idx > 15){
        xpsAlert("Algo de errado. Admin necessário.");
        return;
    }
    myHttpGet('cpTstRetIdx.r'+idx.toString(16) ,capTest_loadHistIndex_callback);
};
//-------------------------------------------------------------------------------------------------

function capTest_loadHistIndex_callback(text)
{
    var t;
    var ty;
    var p;
    var h;
    var resul;
    var it;
    var f = [0,0,0,0,0,0];
    if(text.length == 2){
        xpsAlert("Algo de errado. Admin necessário.(1)");
        return;
    }
    t = text.split("*");
    if(t.length != 2){
        xpsAlert("Algo de errado. Admin necessário.(2)");
        return;
    }

    // Get test type
    ty = parseInt(t[0]); 

    p = t[1].split(",");
    if(p.length != 13){
        xpsAlert("Algo de errado. Admin necessário.(3)");
        return;
    }

    h = gCapTestResult;

    for(var i=1; i<p.length-1;i++){
        p[i] = parseInt(p[i],16);
    }

    h[0].innerHTML  = p[0];                                     // Get name 
    it              = p[1];                                     // Get init type
    h[1].innerHTML  = p[2];                                     // Get finish type
    h[2].innerHTML  = formatUtcMiliSec(p[3]*1000);              // Get init Time
    h[3].innerHTML  = formatUtcMiliSec(p[4]*1000);              // Get end time 
    h[4].innerHTML  = p[5];                                     // Get number of points
    h[5].innerHTML  = (p[6]/10).toFixed(1) + ' V';              // Get final tension
    h[6].innerHTML  = (p[7]/10).toFixed(1) + ' Ah';             // Get discharge rate (accumulated AH)
    h[7].innerHTML  = p[8];                                     // Get sample rate
    h[8].innerHTML  = ty==0?'- x -':(p[9]/10).toFixed(1) + '%'; // Get capacity (FULL test only)
    h[9].innerHTML  = (p[10]/10).toFixed(1) + ' A';             // Get mean current
    h[10].innerHTML = p[11].toString() + ' °C';                 // Get mean bat temperature
    h[11].innerHTML = local_formatTimeFromSec(p[4] - p[3]);     // Get duration 
    h[12].innerHTML = p[12]

    gSampleRateInSeconds = p[8];

    gResultDataHeader  = 'Local: '              + p[0] + '\n';              // Get local
    gResultDataHeader += 'Inicio: '             + h[2].innerHTML + '\n';    // Get init Time
    gResultDataHeader += 'Termino: '            + h[3].innerHTML + '\n';    // Get end time 
    gResultDataHeader += 'Duracao: '            + h[11].innerHTML +'\n';    // Get duration 
    gResultDataHeader += 'Pontos: '             + h[4].innerHTML + '\n';    // Get num points
    gResultDataHeader += 'Tensao Final: '       + h[5].innerHTML + '\n';    // Get Final tension
    gResultDataHeader += 'Taxa Descarga: '      + h[6].innerHTML + '\n';    // Get discharge rate 
    gResultDataHeader += 'Intervalo: '          + h[7].innerHTML + '\n';    // Getsample rate 
    gResultDataHeader += 'Capacidade: '         + h[8].innerHTML + '\n';    // Get capacity 
    gResultDataHeader += 'Corrente Media: '     + h[9].innerHTML + '\n';    // Get mean current
    gResultDataHeader += 'Temperatura media: '  + h[10].innerHTML +'\n\n';  // Get mean temperature 


    // Get the finalization type.
    switch(p[2]){
    case 0:
        resul = "Teste Rápido Cancelado (USCC)";
        f[0] = 2;
        break;
    case 1:
        resul = "Teste Rápido Cancelado (WEB)";
        f[1] = 2;
        break;
    case 2:
        resul = "VCA Restaurado";        
        f[2] = 2;
        break;
    case 3:
        resul = "Teste Rápido Reprovado";        
        f[5] = 2;
        break;
    case 4:
        resul = "Teste Rápido Aprovado";        
        f[3] = 1;
        break;

    case 5:
        resul = "Teste Rápido Aprovado";        
        f[4] = 1;
        break;
    case 6:
        if(p[9] > 800){
            resul = "Teste Completo Aprovado";
        }
        else{
            resul = "Teste Completo Reprovado";
        }
        f[5] = 1;
        break;

    case 7:
        resul = "Teste Completo Falha";
        f[3] = 2;
        break;

    case 8:
        resul = "Teste Completo Cancelado (USCC)";
        f[0] = 2;
        break;
    case 9:
        resul = "Teste Completo Cancelado (WEB)";
        f[1] = 2;
        break;
    
    case 10:
        resul = "Falha Condição inicial";
        break;

    case 11:
        resul = "Teste de Capacidade desabilitado";
        break;

    case 12:
        resul = "Erro de Tabela";
        break;

    case 13:
        resul = "Teste Completo Reprovado";
        break;
    }
    
    h[1].innerHTML = resul;
    captestResult_iniTable.innerHTML = capTest_buildIniTypeTable(it-1);;
    capTest_buildResultTypeTable(f);
    // Get the test data 
    capTest_getDataBuffer();
};
//-------------------------------------------------------------------------------------------------

function capTest_fillTable(data)
{
    var tableBody = getElid('captestResult_data');
    var content = '';
    for (var i = 0; i < data.length; i++) {
        content += '<tr>'
            + '<td>' + formatUtcMiliSec(data[i][0]) + '</td>' // FIXME data format
            + '<td>' + data[i][1] + '</td>'
            + '<td>' + data[i][2] + '</td>'
            + '<td>' + data[i][3] + '</td>'
            + '<td>' + data[i][4] + '</td>'
            + '<td>' + data[i][5] + '</td>'
            + '</tr>';
    }
    tableBody.innerHTML = content
}
//-------------------------------------------------------------------------------------------------

function capTest_drawCharts(data,xLen)
{
    if (data.length === 0){
        xpsAlert("Algo de errado. Admin necessário.");
        return;
    }

    var powerId = 'captestResult_power';
    var tempId = 'captestResult_temp'

    // charts dimensions: full width & half the height of parent
    var power = getElid(powerId)
    var temp = getElid(tempId)
    var h = power.parentElement.clientHeight / 2
    var w = power.parentElement.clientWidth
    power.height = h
    power.width = w
    temp.height = h
    temp.width = w

    // charts boundaries: assuming the data are ordered in time
    var start = data[0][0]
    var end = data[data.length - 1][0]

    var powerData = data.map(function(d) {
        return {
            x: d[0],
            y: [d[1], d[2]]
        }
    });
    var tempData = data.map(function(d) {
        return {
            x: d[0],
            y: [d[3], d[4], d[5]]
        }
    });
   
    chart({
        id: powerId,
        data: powerData,
        yAxisLabel: ['Tensão (V)', 'Corrente (A)'],
        timeSeries: true,
        lineColor: ['blue', 'red'],
        legend: ['VBat', 'IBat'],
        linkGaps: false,
        gapWidth: xLen,
        range: [start, end]
    });

    chart({
        id: tempId,
        data: tempData,
        yAxisLabel: 'Temperatura ºC',
        timeSeries: true,
        lineColor: ['blue', 'red', 'orange'],
        legend: ['TBat', 'Temp1', 'Temp2'],
        linkGaps: false,
        gapWidth: xLen,
        range: [start, end]
    });
}
//-------------------------------------------------------------------------------------------------

// Called by button#captestResult_export
function capTest_exportToCsv(data) {

    var csv = convertArraysToCsv(data)
    
    // Trigger download
    var blob = new Blob(["\uFEFF", csv], {type: 'text/csv'}) // "\uFEFF" is Needed only if there are accented characters
    var filename = 'Capacity-Test-History.csv';
    if (window.Blob && window.navigator.msSaveOrOpenBlob) {
        // Falls to msSaveOrOpenBlob if download attribute is not supported
        window.navigator.msSaveBlob(blob, filename);
    } else {
        var link = document.createElement('a')
        link.setAttribute('href', window.URL.createObjectURL(blob));
        link.setAttribute('download', filename);
        link.dispatchEvent(new MouseEvent('click', {bubbles: true, cancelable: true, view: window}));
    }
};
//-------------------------------------------------------------------------------------------------

function convertArraysToCsv(data) {
    if (data.length === 0){
        xpsAlert("Algo de errado. Admin necessário.(10)");
        return;
    }

    var col = ';';
    var lin = '\n';
    var csv = '';

    csv += gResultDataHeader;
    csv += 'Data' + col
        + 'Hora' + col
        + 'VBat' + col
        + 'IBat' + col
        + 'TBat' + col
        + 'Temp1' + col
        + 'Temp2' + lin;

    for (var i = 0; i < data.length; i++) {
        var datetime = formatUtcMiliSec(data[i][0]).split(' - ')
        csv += datetime[0] + col
            + datetime[1] + col
            + data[i][1] + col
            + data[i][2] + col
            + data[i][3] + col
            + data[i][4] + col
            + data[i][5] + lin;
    }

    return csv;
};

//-------------------------------------------------------------------------------------------------



//----------------------------------
//---   Discharge Table Screen   ---
//----------------------------------
function capTest_loadDischargeTable(idx,n)
{
    var s = "";
    for(var i=0; i<60; i++){
        s += '<tr><th>'+ (i+1);
        s += '</th><td><input type="text" value='+ gCapTest_dTableMin[idx][i] + ' id="dTableMinCel_'+ idx +'_'+ i + '"></td><td><input id="dTableAhCel_'+ idx +'_'+ i + '" type="text" value=';
        s += parseFloat(gCapTest_dTableAH[idx][i]/10).toFixed(1);        
        s += '></td></tr>';
    }

    gCapTestDiscTableNames[idx].value = n;
    gCapTestDiscTables[idx].innerHTML = s;
};
//-------------------------------------------------------------------------------------------------

function capTest_discTablesBtZero()
{
    var i = gDiscTableSelIdx;

    gCapTest_dTableMin[i].fill(0);
    gCapTest_dTableAH[i].fill(0);
    capTest_loadDischargeTable(i,"Tabela "+ (i+1).toString());
};
//-------------------------------------------------------------------------------------------------

function capTest_discTablesBtDefault()
{
    var i = gDiscTableSelIdx;
    var a = gCapTest_dTableMin[i];
    var b = gCapTest_dTableAH[i];

    a[0]=12;
    a[1]=18;
    a[2]=24;
    a[3]=30;
    a[4]=45;
    a[5]=60;
    a[6]=120;
    a[7]=180;
    a[8]=240;
    a[9]=300;
    a[10]=360;
    a[11]=420;
    a[12]=480;
    a[13]=540;
    a[14]=600;
    a[15]=900;
    a[16]=1200;
    a[17]=1800;
    a[18]=2400;
    a[19]=3000;
    a[20]=6000;

    b[0]=146;
    b[1]=167;
    b[2]=189;
    b[3]=210;
    b[4]=260;
    b[5]=281;
    b[6]=347;
    b[7]=394;
    b[8]=420;
    b[9]=433;
    b[10]=442;
    b[11]=451;
    b[12]=469;
    b[13]=469;
    b[14]=475;
    b[15]=498;
    b[16]=517;
    b[17]=558;
    b[18]=570;
    b[19]=580;
    b[20]=620;
    capTest_loadDischargeTable(i,"Tabela "+ (i+1).toString());
};
//-------------------------------------------------------------------------------------------------

function capTest_discTablesBtRead()
{
    myHttpGet('reaDdTa.txc',capTest_discTablesBtRead_callback);
};
//-------------------------------------------------------------------------------------------------

function capTest_discTablesBtRead_callback(text)
{
    var ts  = text.split(";");
    if(ts.length != 6){
        xpsAlert("Erro: Número de tabelas.");
        return;
    }

    for(var t=0; t<5; t++){
        var s   = ts[t].split("!");
        // Get the name
        var n   = s[0];
        // Get the values
        var v   = s[1].split(",");
        var j;
    
        // Verify the number of values.
        if(v.length != 121){
            xpsAlert("Error.");
            return;
        }
        
        for(var i=0, j=0; i<60;i++){
            gCapTest_dTableMin[t][i] = parseInt(v[j++],16);
            gCapTest_dTableAH[t][i]  = parseInt(v[j++],16);
        }
        // Fill tha table on screen
        capTest_loadDischargeTable(t,n);
        local_clearBorder(gCapTestDiscTableNames[gDiscTableSelIdx]);
    }
};
//-------------------------------------------------------------------------------------------------

function capTest_discTablesBtSave()
{
    var dp = getDecimalSeparator();
    var s   = '';
    var mCel= "dTableMinCel_";
    var aCel= "dTableAhCel_";
    var t   = gDiscTableSelIdx; // Get the table that is on the screen

    s += gCapTestDiscTableNames[t].value;// Get name
    s = s.concat('!');

    for(var i=0; i<60; i++){
        var iC = t.toString().concat('_').concat(i.toString());
        var c  = mCel.concat(iC);
        var a  = aCel.concat(iC);
        var sm = getElid(c).value;
        var sa = getElid(a).value;
    
        sa = sa.replace(',','.');
        // Get correct decimal separator
//        if(dp === "."){
//            sa = sa.replace(',',dp);
//        }
//        else{
//            sa = sa.replace('.',dp);
//        }
        
        sm = parseInt(sm).toString(16);
        sa = (parseFloat(sa)*10).toString(16);
        s = s.concat(sm+','); // Get minute
        s = s.concat(sa+','); // Get AH
    }
    s = s.concat(';'); // Separate tables


    pLog(s);
    myHttpPost('neewDisTa'+gDiscTableSelIdx.toString()+ s, capTest_discTablesBtSave_callback);
};
//-------------------------------------------------------------------------------------------------

function capTest_discTablesBtSave_callback(text)
{
    local_clearBorder(gCapTestDiscTableNames[gDiscTableSelIdx]);
    xpsAlert("Tabela salva com sucesso");
};
//-------------------------------------------------------------------------------------------------

function capTest_btStartTest()
{
    myHttpPost('capTestInit', capTest_btStartTest_callback);
};
//-------------------------------------------------------------------------------------------------

function capTest_btStartTest_callback(text)
{
    xpsAlert(text);
};
//-------------------------------------------------------------------------------------------------

function capTest_btCancelTest()
{
    myHttpPost('capTestCancel', capTest_btCancelTest_callback);
};
//-------------------------------------------------------------------------------------------------

function capTest_btCancelTest_callback(text)
{
    xpsAlert(text);
};
//-------------------------------------------------------------------------------------------------

function capTest_monit(x)
{
    myHttpGet('capTestMonit',capTest_monit_callback);
};
//-------------------------------------------------------------------------------------------------

function capTest_monit_callback(text)
{
    pLog(text);
    // Hide all screens
    captestCurrent_on.classList   = "hide";
    captestCurrent_wait.classList = "hide";
    captestCurrent_off.classList  = "hide";
    captestCurrent_reprov.classList = "hide";

    var v = text.split(',');
    if(v.length != 18){
        captestCurrent_off.classList  = "";
        return;
    }

    var t = parseInt(v[0],16);
    var r = parseInt(v[1],16);
    var d = parseInt(v[2],16);
    var vb = parseInt(v[3],16);
    var ib = parseInt(v[4],16);
    var tb = parseInt(v[5],16);
    var im = parseInt(v[6],16);
    var ahacc = parseInt(v[7],16);
    var tm = parseInt(v[8],16);
    var cd = parseInt(v[9],16);         // Periodic count down
    var it = parseInt(v[10],16) - 1;    // Init type
    var tp = parseInt(v[11],16);        // Test type
    var td = parseInt(v[12],16);        // Test disapproved flag

    capTable_dropSel0.innerHTML = v[13];
    capTable_dropSel1.innerHTML = v[14];
    capTable_dropSel2.innerHTML = v[15];
    capTable_dropSel3.innerHTML = v[16];
    capTable_dropSel4.innerHTML = v[17];

    if(gTestRunningFlag != t){
        gTestRunningFlag = t;
        if(t==0){
            capTest_paramBtRead();
        }
    }

    if (td != 0){
        capTest_runScr_hide();
        captestCurrent_reprov.classList = "";
        return;
    }

    // If there is reserve time, show it on the screen. 
    if(r != 0){
        var inHtml = "";
        captestCurrent_wait.classList = "";
        capTest_runScr_hide();
        if(cd != 0){
            inHtml = "    (Reserva  : " + local_formatTimeFromSec(r)+")";
            inHtml += "<br>(Periódico: " + local_formatTimeFromSec(cd)+")";
            captestRunCard_wait.innerHTML =inHtml;
            return;
        }

        captestRunCard_wait.innerHTML = local_formatTimeFromSec(r) +inHtml;
        return;
    }

    if (t == 0){
        capTest_runScr_hide();
        captestCurrent_off.classList  = "";
        if(cd != 0){
            captestCurrent_perCD.innerHTML = "Inicio de periodico em:<br>" + local_formatTimeFromSec(cd);
        }
        else{
            captestCurrent_perCD.innerHTML = "Nenhum teste programado";
        }
        return;
    }

    // Get the string for Fast or Full test.
    tpS = tp==0?"Rápido":"Completo";
    // Get the real value of current and temperature. 
    if(ib >= 0x80000000){
        ib = 0x100000000-ib;
        ib *= -1;
    }
    if(tb >= 0x80000000){
        tb = 0x100000000-tb;
        tb *= -1;

        if(tb == -201){
            tb ='Err';
        }
        else if(tb == -202){
            tb = 'Inv';
        }
    }
    if(im >= 0x80000000){
        im = 0x100000000-im;
        im *= -1;
    }
    if(tm >= 0x80000000){
        tm = 0x100000000-tm;
        tm *= -1;
    }
    if(ahacc >= 0x80000000){
        ahacc = 0x100000000-ahacc;
        ahacc *= -1;
    }
    vb = (vb/10).toFixed(1);
    ib = (ib/10).toFixed(1);    
    im = (im/10).toFixed(1);    
    ahacc = (ahacc/10).toFixed(1);    
    // Fill the "test running" Screen
    capTest_runScr_tl.innerHTML = "Teste de capacidade: " + tpS;
    capTest_runScr_vb.innerHTML = vb;// + " V";
    capTest_runScr_ib.innerHTML = ib;// + " A";
    capTest_runScr_tb.innerHTML = tb;// + " ºC";
    capTest_runScr_im.innerHTML = im;
    capTest_runScr_tm.innerHTML = tm;
    capTest_runScr_du.innerHTML = local_formatTimeFromSec(d);
    capTest_runScr_ca.innerHTML = ahacc;
    captestCurrent_iniTable.innerHTML = capTest_buildIniTypeTable(it);

    captestCurrent_on.classList = "";
    captestRunCard_start.innerHTML = local_formatTimeFromSec(d);
};
//-------------------------------------------------------------------------------------------------

function capTest_paramBtRead()
{
    myHttpGet('rCapPAr.txc',capTest_paramBtRead_callback);
};
//-------------------------------------------------------------------------------------------------

function capTest_paramBtRead_callback(text)
{
    var ts  = text.split(",");
    if(ts.length != 12){
        xpsAlert("Erro: Formato da resposta.");
        return;
    }

    for(var i=0; i<gcapTestParamsValues.length;i++){
        gcapTestParamsValues[i] = parseInt(ts[i],16);
    }

    capTest_buildScreen();
    capTest_en();
    capTest_paramHistoryRead(); // Read the history table.
};
//-------------------------------------------------------------------------------------------------

function capTest_btTestRelease()
{
    myHttpPost('capTestRelease', capTest_btTestRelease_callback);
}
//-------------------------------------------------------------------------------------------------

function capTest_btTestRelease_callback(t)
{
    pLog(t);
}
//-------------------------------------------------------------------------------------------------

function capTest_paramBtSave()
{
    var s = '';
    var r;

    r = capTest_readParamScreen();
    switch(r){
    case 1:
        // Intervalo (dias) [1 a 255]
        xpsAlert("Intervalo inválido: [1 a 255]");
        return;
        
    case 2:
        // Tensao Final     [450 a 520]
        xpsAlert("Tensão final inválida: [45,0 a 52,0]");
        return;

    case 3:
        // Descarga maxima  [10 a 9999]
        xpsAlert("Descarga máxima inválida: [1,0 a 999,9]");
        return;

    case 4:
        // T max            [1 a 2880]
        xpsAlert("Tempo máximo inválido: [1 a 2888]");
        return;

    case 5:
        // T reserva        [1 a 3600]    
        xpsAlert("Tempo reserva inválido: [1 a 3600]");
        return;
    }


    for(var i=0; i<gcapTestParamsValues.length; i++){
        s = s.concat(gcapTestParamsValues[i].toString(16)+','); 
    }
    myHttpPost('ctneEwPar' + s + ';', capTest_paramBtSave_callback);
};
//-------------------------------------------------------------------------------------------------

function capTest_paramBtSave_callback(text)
{
    local_clearAllBorders();
    xpsAlert("Salvo com sucesso");
};
//-------------------------------------------------------------------------------------------------

function capTest_runScr_show()
{
    capCurrentShowModal();
};
//-------------------------------------------------------------------------------------------------

function capTest_runScr_hide()
{
    capCurrentHideModal();
};
//-------------------------------------------------------------------------------------------------
function capTest_saveCsv() 
{
    capTest_exportToCsv(gResultDataArray);
};
//-------------------------------------------------------------------------------------------------


function capTest_init()
{
    getElid("cParamType").addEventListener("change", function(){
        capTest_fullEnabled();
    });
};
//-------------------------------------------------------------------------------------------------

capTest_init();