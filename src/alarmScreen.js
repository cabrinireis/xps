var gALMS_inter  = getElid("alacgint");
var gALMS_relay1 = document.querySelectorAll('.alsRele1');
var gALMS_relay2 = document.querySelectorAll('.alsRele2');
var gALMS_relay3 = document.querySelectorAll('.alsRele3');
var gALMS_relay4 = document.querySelectorAll('.alsRele4');
var gALMS_relay5 = document.querySelectorAll('.alsRele5');
var gALMS_relay6 = document.querySelectorAll('.alsRele6');
var gALMS_relay7 = document.querySelectorAll('.alsRele7');
var gALMS_relay8 = document.querySelectorAll('.alsRele8');
var gALMS_severity = document.querySelectorAll('.form-control.als');
var gALMS_trap   = document.querySelectorAll('.alsTrap');
var gALMS_buzz   = document.querySelectorAll('.alsSonoro');
var gALMS_mail   = document.querySelectorAll('.alsMail');
var gALMS_relay  = [gALMS_relay1,gALMS_relay2,gALMS_relay3,gALMS_relay4,gALMS_relay5,gALMS_relay6,gALMS_relay7,gALMS_relay8];

var gALMS_alarmScreenLb = document.querySelectorAll('.Alr-Infra-Label');


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

function local_setColum(columIn, numLin, value)
{
    let i;
    for(i=0; i<numLin; i++){
        columIn[i].checked = value&(1<<i)?1:0;
    }
};
//-------------------------------------------------------------------------------------------------

function alarm_setLabels(idx,lbIn)
{
    if (idx >= gALMS_alarmScreenLb.length){
        return;
    }
    gALMS_alarmScreenLb[idx].innerHTML = lbIn;
};
//-------------------------------------------------------------------------------------------------

function alarmSaveButton()
{
    let s = "";
    let v = 0;
    let i = 0;
    let j = 0;
    let seve = 0;
    let trap = 0;
    let buzz = 0;
    let mail = 0;

    // Interval
    s += gALMS_inter.selectedIndex;
    s = s.concat("*");

    // Relays
    for(j=0; j<8; j++){    
        for(i=0, v=0; i<16; i++){
            v |= gALMS_relay[j][i].checked?(1<<i):0;
        }   
        s = s.concat(v.toString(16));
        s = s.concat("*");
    }

    for(i=0, v=0; i<16; i++){
        seve |= gALMS_severity[i].selectedIndex <<(i*2);
        trap |= gALMS_trap[i].checked?(1<<i):0;
        buzz |= gALMS_buzz[i].checked?(1<<i):0;
        mail |= gALMS_mail[i].checked?(1<<i):0;
    }   

    s = s.concat(seve.toString(16));
    s = s.concat("*");
    s = s.concat(trap.toString(16));
    s = s.concat("*");
    s = s.concat(buzz.toString(16));
    s = s.concat("*");
    s = s.concat(mail.toString(16));

    pLog(s);
    myHttpPost('alarm==/'+ s, alarmSaveButtonCallback);
};
//-------------------------------------------------------------------------------------------------

function alarmSaveButtonCallback(text)
{
    if(text == "OK"){
        xpsAlert("Alterações salvas com sucesso.");
    }
    pLog(text);
};
//-------------------------------------------------------------------------------------------------

function alarmReadButton()
{
    myHttpGet('alarm.txt',alarmReadButtonCallback);
};
//-------------------------------------------------------------------------------------------------

function alarmReadButtonCallback(text)
{
    let i;
    let seve;
 
    var values = text.split("*");
    pLog(values);

    gALMS_inter.selectedIndex = parseInt(values[0],16);

    // Paint Relays
    for(i=0; i<8; i++){
        local_setColum(gALMS_relay[i],16,parseInt(values[i+1],16))
    }

    seve = parseInt(values[9],16)
    for(i=0; i<16; i++){
        gALMS_severity[i].selectedIndex = (seve & 0x03);
        seve >>= 2;
    }

    local_setColum(gALMS_trap,16,parseInt(values[10],16)); // Paint Traps
    local_setColum(gALMS_buzz,16,parseInt(values[11],16)); // Paint Buzzer
    local_setColum(gALMS_mail,16,parseInt(values[12],16)); // Paint Mails
};
//-------------------------------------------------------------------------------------------------

function alarmClearAll()
{
    let i;
    // Clear Relays
    for(i=0; i<8; i++){
        local_setColum(gALMS_relay[i],16,0);
    }
    // Clear Severity, Traps, Buzzer and Email
    for(i=0; i<16; i++){
        gALMS_severity[i].selectedIndex  = 0; 

        gALMS_trap[i].checked = 0;
        gALMS_buzz[i].checked = 0;
        gALMS_mail[i].checked = 0;
    }
};
//-------------------------------------------------------------------------------------------------

function alarmRestoreDefalut()
{
    let i;
    gALMS_severity[7].selectedIndex  = 3; // VCC Alta:             Urgente e sonoro. 
    gALMS_severity[8].selectedIndex  = 2; // Falha CA:             Não urgente. 
    gALMS_severity[9].selectedIndex  = 3; // Fusível aberto:       Urgente e sonoro.
    gALMS_severity[10].selectedIndex = 3; // Falha UR:             Urgente e sonoro. 
    gALMS_severity[11].selectedIndex = 3; // Bateria desconectada: Urgente e sonoro.
    gALMS_severity[12].selectedIndex = 3; // Bateria em descarga:  Urgente.
    gALMS_severity[13].selectedIndex = 1; // Bateria em carga:     Advertência.

    for(i=7; i<13; i++){
        gALMS_trap[i].checked = 0;
        gALMS_buzz[i].checked = 0;
        gALMS_mail[i].checked = 0;
    }

    gALMS_buzz[7].checked  = 1;
    gALMS_buzz[9].checked  = 1;
    gALMS_buzz[10].checked = 1;
    gALMS_buzz[11].checked = 1;
};
//-------------------------------------------------------------------------------------------------
