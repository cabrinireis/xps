var mTableH = getElid("mHistHead");
var mTableB = getElid("mHistBody");

var hStrings = [
	"Falha nao cadastrada 1",
	"Falha nao cadastrada 2",
	"USCC Reiniciada",
	"Disjuntor/Fusível de Bateria 1 - Inicio",
	"Disjuntor/Fusivel de Bateria 1 - Fim",
	"Disjuntor/Fusível de Bateria 2 - Inicio",
	"Disjuntor/Fusivel de Bateria 2 - Fim",
	"Disjuntor/Fusível de Consumidor - Inicio",
	"Disjuntor/Fusivel de Consumidor - Fim",
	"Bateria em descarga - Inicio",
	"Bateria em descarga - Fim",				// 10
	"Desconexão de Bateria - Inicio",
	"Desconexão de Bateria - Fim",
	"Bateria em Carga - Inicio",
	"Bateria em carga - Fim",
	"CA Anormal - Inicio",
	"CA Anormal - Fim",
	"Falha USCC - Inicio",
	"Falha USCC - Fim",
	"Falha de 1 UR - Inicio",
	"Falha de 1 UR - Fim",						// 20
	"Falha de Multiplas URs - Inicio",
	"Falha de Multiplas URs - Fim",
	"Tensão de Consumidor Alta - Inicio",
	"Tensão de Consumidor Alta - Fim",
	"Entrada [Infraestrutura 1] - Inicio",
	"Entrada [Infraestrutura 1] - Fim",
	"Entrada [Infraestrutura 2] - Inicio",
	"Entrada [Infraestrutura 2] - Fim",
	"Entrada [Infraestrutura 3] - Inicio",
	"Entrada [Infraestrutura 3] - Fim",			// 30
	"Entrada [Infraestrutura 4] - Inicio",
	"Entrada [Infraestrutura 4] - Fim",
	"Entrada [Infraestrutura 5] - Inicio",
	"Entrada [Infraestrutura 5] - Fim",
	"Entrada [Temperatura 1] - Inicio",
	"Entrada [Temperatura 1] - Fim",
	"Entrada [Temperatura 2] - Inicio",
	"Entrada [Temperatura 2] - Fim",
    "Carga Manual - Inicio",
    "Carga Manual - Fim",
    "Carga - Fim de 10 horas",
    "Reposicao Automatica",
    "Reposicao Manual",
	"Reposicao Pela WEB",
	"Simetria alta Bat4 banco 1 Inicio",
	"Simetria alta Bat4 banco 1 Fim",
	"Simetria alta Bat3 banco 1 Inicio",
	"Simetria alta Bat3 banco 1  Fim",
	"Simetria alta Bat2 banco 1 Inicio",
	"Simetria alta Bat2 banco 1 Fim",
	"Simetria alta Bat1 banco 1 Inicio",
	"Simetria alta Bat1 banco 1 Fim",
	"Simetria baixa Bat4 banco 1 Inicio",
	"Simetria baixa Bat4 banco 1 Fim",
	"Simetria baixa Bat3 banco 1 Inicio",
	"Simetria baixa Bat3 banco 1 Fim",
	"Simetria baixa Bat2 banco 1 Inicio",
	"Simetria baixa Bat2 banco 1 Fim",
	"Simetria baixa Bat1 banco 1 Inicio",
	"Simetria baixa Bat1 banco 1 Fim",			// 60
	"Relé manual 1 Inicio",
	"Relé manual 1 Fim",
	"Relé manual 2 Inicio",
	"Relé manual 2 Fim",
	"Relé manual 3 Inicio",
	"Relé manual 3 Fim",
	"Relé manual 4 Inicio",
	"Relé manual 4 Fim",
	"Relé manual 5 Inicio",
	"Relé manual 5 Fim",						// 70
	"Relé manual 6 Inicio",
	"Relé manual 6 Fim",
	"Relé manual 7 Inicio",
	"Relé manual 7 Fim",
	"Simetria alta Bat4 banco 2 Inicio",
	"Simetria alta Bat4 banco 2 Fim",
	"Simetria alta Bat3 banco 2 Inicio",
	"Simetria alta Bat3 banco 2  Fim",
	"Simetria alta Bat2 banco 2 Inicio",
	"Simetria alta Bat2 banco 2 Fim",			// 80
	"Simetria alta Bat1 banco 2 Inicio",
	"Simetria alta Bat1 banco 2 Fim",
	"Simetria baixa Bat4 banco 2 Inicio",
	"Simetria baixa Bat4 banco 2 Fim",
	"Simetria baixa Bat3 banco 2 Inicio",
	"Simetria baixa Bat3 banco 2 Fim",
	"Simetria baixa Bat2 banco 2 Inicio",
	"Simetria baixa Bat2 banco 2 Fim",
	"Simetria baixa Bat1 banco 2 Inicio",
	"Simetria baixa Bat1 banco 2 Fim", 			// 90
	"Teste de capacidade ON: USCC",
	"Teste de capacidade ON: WEB",
	"Teste de capacidade ON: AUTO",
	"Teste de capacidade ON: PERIODICO",
	"Teste de capacidade. Falha ao iniciar: bateria em carga",
	"Teste de capacidade. Falha ao iniciar: bateria desconectada",
	"Teste de capacidade. Falha ao iniciar: bateria com fusível aberto",
	"Teste de capacidade. Falha ao iniciar: VCA em falha",
	"Teste de capacidade rápido. [CANCELADO] USCC",
	"Teste de capacidade rápido. [CANCELADO] WEB",	// 100
	"Teste de capacidade rápido. [FALHA] VCA Restaurado",
	"Teste de capacidade rápido. [REPROVADO] Tensão Final",
	"Teste de capacidade rápido. [APROVADO] Descarga Máxima",
	"Teste de capacidade rápido. [APROVADO] Tempo Máximo",
	"Troca do link da ethernet: velocidade",
	"Troca do link da ethernet: mode de operação",
	"Teste de capacidade completo. [FALHA] Tempo máximo",
	"Teste de capacidade desabilitado",
	"Teste de capacidade. Falha ao iniciar: Tensão inferior a permitida",
	"Teste de capacidade completo. [CANCELADO] USCC", // 110
	"Teste de capacidade completo. [FALHA] Tabela inválida", 
	"Erro na leitura dos testes de capacidade",
	"Erro na formatação da flash para teste de capacidade",
	"Relógio atualizado",
	"Erro na inicialização da flash",
	"Fim de carga de bateria por timeout",
	"Reiniciado por falta de comunicação HTTP",
	"Reiniciado por falta de comunicação SNMP",
	"Reiniciado por comando SNMP"
];

function formatUtcMiliSec(milsecs) {
    var eD;	
    var t = new Date(milsecs);
    t.setTime(t.getTime() + (t.getTimezoneOffset()*1000*60));
	y = t.getFullYear();
	m  = t.getMonth()+1;
	d  = t.getDate(); 
	h  = t.getHours();
	mi = t.getMinutes();
	s  = t.getSeconds(); 
	eD = "";
	if(d < 10){
		eD += "0";
	}
	eD += d + "/";
	if(m<10){
		eD += "0";
	}
	eD += m + "/" +y;
	eD += " - "; 
	if(h < 10){
		eD += "0";
	}
	eD += h + ":";
	if(mi<10){
		eD += "0";
	}
	eD += mi + ":";
	if(s<10){
		eD += "0";
	}
	eD += s;
	return eD;
};
//-------------------------------------------------------------------------------------------------

function formatUtcSec(seconds) {
    var eD;	
    var t = new Date(seconds*1000);
    t.setTime(t.getTime() + (t.getTimezoneOffset()*1000*60));
	y = t.getFullYear();
	m  = t.getMonth()+1;
	d  = t.getDate(); 
	h  = t.getHours();
	mi = t.getMinutes();
	s  = t.getSeconds(); 
	eD = "";
	if(d < 10){
		eD += "0";
	}
	eD += d + "/";
	if(m<10){
		eD += "0";
	}
	eD += m + "/" +y;
	eD += " - "; 
	if(h < 10){
		eD += "0";
	}
	eD += h + ":";
	if(mi<10){
		eD += "0";
	}
	eD += mi + ":";
	if(s<10){
		eD += "0";
	}
	eD += s;
	return eD;
};
//-------------------------------------------------------------------------------------------------

function hist_setVccInputStrings()
{
	hStrings[15] = "VCCin Anormal - Inicio";
	hStrings[16] = "VCCin Anormal - Fim";
	hStrings[19] = "Falha de 1 UC - Inicio";
	hStrings[20] = "Falha de 1 UC - Fim";
	hStrings[21] = "Falha de Multiplas UCs - Inicio";
	hStrings[22] = "Falha de Multiplas UCs - Fim";
	hStrings[98]  = "Teste de capacidade. Falha ao iniciar: VCCin em falha";
	hStrings[101] = "Teste de capacidade rápido. [FALHA] VCCin Restaurado";
};
//-------------------------------------------------------------------------------------------------

function bulidHistoryDescriptionForCapTest(value)
{
	if(value < hStrings.length){
		return hStrings[value];
	}
	else if(value < 0x8000 || value > (0x8000+1000)){
		return "Desconhecido";
	}else {
		value -= 0x8000;
		if(value < 800){
			return "Teste de capacidade Completo REPROVADO. Capacidade: " + (value/10).toFixed(1) + "% ";
		}
	}
	return "Teste de capacidade Completo APROVADO. Capacidade: " + (value/10).toFixed(1) + "% ";
}
//-------------------------------------------------------------------------------------------------

function buildTable(tableData)
{
	var eventNum;
	var desc;

	mTableH.innerHTML = '<thead><tr><th class="tab-line">#</th><th class="data-col">Data</th><th class="hour-col">Hora</th><th class="cod-col">Código</th><th class="stts-col">Status</th><th class="descr-col">Descrição</th></tr></thead>';
    
	for(var i=0; i<tableData.length; i++){
		var s = tableData[i].split(":");
        r = formatUtcMiliSec(parseInt(s[0],16)*1000);
		r = r.split('-');
		date = r[0];
		time = r[1];
		eventNum = parseInt(s[1],16);
		desc = bulidHistoryDescriptionForCapTest(eventNum);

		stat = "Inicio";
		if(eventNum % 2 == 0){
			stat = "Fim";	
        }

        var timeN = getElid("hm").getAttribute("data-timeutc");

        if(timeN != null){
            var tDiff = parseInt(timeN,16) - parseInt(s[0],16)
        }
        else{
            var tDiff = 301;
        }
		
        if(tDiff <= 10){
            mTableB.innerHTML += '<tr class="tens"><td class="tab-line">'+ (i+1) +'</td><td class="data-col">' + date + '</td><td class="hour-col">' + time + '</td><td class="cod-col">' + s[1] + '</td><td class="stts-col">' + stat + '</td><td class="descr-col">' + desc + '</td></tr>';
        }
        else if(tDiff <= 60){
            mTableB.innerHTML += '<tr class="onem"><td class="tab-line">'+ (i+1) +'</td><td class="data-col">' + date + '</td><td class="hour-col">' + time + '</td><td class="cod-col">' + s[1] + '</td><td class="stts-col">' + stat + '</td><td class="descr-col">' + desc + '</td></tr>';
        }
        else if(tDiff <= 300){
            mTableB.innerHTML += '<tr class="fivem"><td class="tab-line">'+ (i+1) +'</td><td class="data-col">' + date + '</td><td class="hour-col">' + time + '</td><td class="cod-col">' + s[1] + '</td><td class="stts-col">' + stat + '</td><td class="descr-col">' + desc + '</td></tr>';
        }
        else if(tDiff > 300){
            mTableB.innerHTML += '<tr class="morem"><td class="tab-line">'+ (i+1) +'</td><td class="data-col">' + date + '</td><td class="hour-col">' + time + '</td><td class="cod-col">' + s[1] + '</td><td class="stts-col">' + stat + '</td><td class="descr-col">' + desc + '</td></tr>';
        }
	}
};
//-------------------------------------------------------------------------------------------------

function histCallback(text)
{
    clearHistScreen();
	buildTable(text.split("*"));
	console.log("OK");
};
//-------------------------------------------------------------------------------------------------

function clearHistScreen(){
	mTableH.innerHTML = '<thead><tr><th class="tab-line">#</th><th class="data-col">Data</th><th class="hour-col">Hora</th><th class="cod-col">Código</th><th class="stts-col">Status</th><th class="descr-col">Descrição</th></tr></thead>';
    mTableB.innerHTML = '';
};
//-------------------------------------------------------------------------------------------------

function histUpdateInputsLabel(i,v)
{
	if(i>=7){
		return;
	}

	i *= 2;  // Each input has 2 labels 
	i += 25; // Offset of the first input label 

	hStrings[i]   = v + " - Inicio";
	hStrings[i+1] = v + " - Fim";
};
//-------------------------------------------------------------------------------------------------

function histUpdateRelaysLabel(i,v)
{
	if(i>=7){
		return;
	}

	i *= 2;  // Each input has 2 labels 
	i += 61; // Offset of the first Relay label 

	hStrings[i]   = v + " - Inicio";
	hStrings[i+1] = v + " - Fim";
};
//-------------------------------------------------------------------------------------------------