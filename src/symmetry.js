var mSYM = [
    getElid("symm_ena"),
    getElid("symm_mod"),
    getElid("symm_hwr"),
    getElid("symm_bat"),
    getElid("symm_hi"),
    getElid("symm_lo"),
    getElid("symm_tab")
];

var seq = document.querySelectorAll(".form-control.simet");

function symCardEn()
{
    if(mSYM[0].checked){
        for(i=0 ; i<seq.length ; i++){
            seq[i].removeAttribute("disabled");
            }
    }
    else {
        for(i=0 ; i<seq.length ; i++){
            seq[i].setAttribute("disabled", "disabled");
        }
    }
};
//-------------------------------------------------------------------------------------------------

function local_threshold(n)
{
    var myInt;
    n.value = n.value.replace(',','.');
    myInt = parseFloat(n.value)*10;
    if(isNaN(myInt)){
        myInt = 0;
    }
    n.value = (myInt/10).toFixed(1).toString();
    return parseFloat(n.value)*10;
}
//-------------------------------------------------------------------------------------------------

function local_buildTable(tab, highLight)
{
    pLog(tab);

    if(highLight==false){
        mSYM[6].innerHTML = "";
        return;    
    }

    var inHtml = "";
    var tabNor = "</td><td>";
    var tabRed = "</td><td class=\"bgcolor-red\">";
    var tabYel = "</td><td class=\"bgcolor-yellow\">";
    for(j=0,i=1; i<5; i++){
        inHtml += "<tr><td>";
        inHtml += i.toString();
        // Bank 1
        inHtml += tab[j]=='2'?tabRed:(tab[j]=='1'?tabYel:tabNor);
        j++;
        inHtml += (parseInt(tab[j++])/10).toFixed(1).toString();
        // Bank 2
        inHtml += tab[j]=='2'?tabRed:(tab[j]=='1'?tabYel:tabNor);
        j++;
        inHtml += (parseInt(tab[j++])/10).toFixed(1).toString();

        inHtml += "</td></tr>";
    }

    mSYM[6].innerHTML = inHtml;
}
//-------------------------------------------------------------------------------------------------
function btWriteSym()
{
    var s     = '';
    var myInt = 0;
    var errMs ="Valor deve estar entre 0.5 e 4.0 volts";

    if(mSYM[0].checked == false){
        s = "0*0*0*0*0*0*";
    }
    else{
        s = "1*";
        s = s.concat(mSYM[1].selectedIndex.toString() + "*");
        s = s.concat(mSYM[2].selectedIndex.toString() + "*");
        s = s.concat(mSYM[3].selectedIndex.toString() + "*");
    
        myInt = local_threshold(mSYM[4]);
        if(myInt >40 || myInt < 5){
            xpsAlert(errMs);
            return;
        }
        s = s.concat(myInt.toString() + "*");
    
        myInt = local_threshold(mSYM[5]);
        if(myInt >40 || myInt < 5){
            xpsAlert(errMs);
            return;
        }
        s = s.concat(myInt.toString() + "*");
    }

    if(mSYM[0].checked){
        mSYM[6].innerHTML = "";
    }

    pLog(s);
    myHttpPost('savsymM'+ s, btWriteSymCallB);
}
//-------------------------------------------------------------------------------------------------

function btWriteSymCallB(text)
{
    mSYM[4].classList.remove('inputChangeBorder');
    mSYM[4].classList.remove('inputErrorBorder');
    mSYM[5].classList.remove('inputChangeBorder');
    mSYM[5].classList.remove('inputErrorBorder');        

    if(text == "OK"){
        xpsAlert("Alterações salvas com sucesso.");
    }
    pLog(text);
}
//-------------------------------------------------------------------------------------------------

function symmetry_getAll()
{
    myHttpGet('symett.txt',symmetry_getAllCallback);
};
//-------------------------------------------------------------------------------------------------

function symmetry_getAllCallback(text)
{
    var v = text.split("!");
    
    pLog("Symmetry Get All");
    pLog(v);

    if(v.length != 7){
        pLog("Error on Symmetry Get All");
        return;
    }


    mSYM[0].checked         = v[0]=="1"?true:false;
    mSYM[1].selectedIndex   = v[1]=="2"?2:1;
    mSYM[2].selectedIndex   = v[2]=="2"?2:1;
    mSYM[3].selectedIndex   = v[3]=="2"?2:1;
    mSYM[4].value           = (parseInt(v[4])/10).toFixed(1);
    mSYM[5].value           = (parseInt(v[5])/10).toFixed(1);

    local_buildTable(v[6].split("*"),mSYM[0].checked);

    symCardEn();

    pLog("OK");
};
//-------------------------------------------------------------------------------------------------

function symmetry_getTensions()
{
    myHttpGet('symett.txt',symmetry_getTensionsCallback);
}
//-------------------------------------------------------------------------------------------------

function symmetry_getTensionsCallback(text)
{
    var v = text.split("!");

    pLog("Symmetry Get Tensions");
    pLog(v);

    if(v.length != 7){
        pLog("Error on Symmetry Get Tensions");
        return;
    }

    local_buildTable(v[6].split("*"),mSYM[0].checked);
}
//-------------------------------------------------------------------------------------------------
