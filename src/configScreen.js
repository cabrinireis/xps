var gConfig_USCC = [
getElid("placehold"),
getElid("configpdbatct"),
getElid("configpdbatlcc"),
getElid("configpdbatca"),
getElid("configpdbatre"),
];

var gConfig_sistema = [
getElid("configsyst"),
getElid("confinurs"),
getElid("configssb"),
getElid("configbtmc"),
getElid("configsysbat")
];

var gConfig_alarmes = [
getElid("configatac"),
getElid("configabatd"),
getElid("configadesbat1"),
getElid("configadesbat2"),
getElid("configatvcab"),
getElid("configatvcaa"),
];

var gConfig_baterias = [
getElid("configpdbattf"),
getElid("configpdbattc"),
getElid("configpdbatcc"),
getElid("configpdbatac"),
getElid("configpdbatrb"),
getElid("configpdbatmvce"),
getElid("configpdbatne"),
getElid("configpdbatlc"),
];

var gConfScr_ursPerSystem = [2,4,2,4,8,12,15,5,10];
var gConfScr_vccInFlag = 0;

var gConfScr_limitValues = 
{
    "Local":"Padrão",

    "Flags":[false,false,false,false],

    "Sistema":
    {
        "numURs":[0,12],
        "Shunt":[1,3000]
    },

    "Alarmes": 
    [
        [520,  630,  10, " (V)"],
        [460,  495,  10, " (V)"],
        [380,  440,  10, " (V)"],
        [380,  440,  10, " (V)"],
        [85,   238,  1,  " (V)"],
        [194,  290,  1,  " (V)"]
    ],
    
    "Baterias":
    [
        [445, 580,  10, " (V)"],
        [460, 590,  10, " (V)"],
        [10,  9999, 10, " (A)"],
        [0,   120,  1,  " (Minutos)"],
        [480, 520,  10, " (V)"],
        [1,   10,   1,  ""],
        [6,   30,   1,  ""],
        [0,   9999, 10, " (V)"]
    ],
};


var gConfig_lastSaveTick;

function config_inputWithBorder(inputIn, value)
{
    inputIn.setAttribute("value",value);
    inputIn.setAttribute("data-olddata",value);
    inputIn.value = value;
}
//-------------------------------------------------------------------------------------------------

function local_clearBorder(bIn)
{
    bIn.classList.remove('inputChangeBorder');
    bIn.classList.remove('inputErrorBorder');
}
//-------------------------------------------------------------------------------------------------

function config_clearBorders()
{
    // Do not include the button
    for(var i = 0; i < gConfig_sistema.length - 1; i++){
        local_clearBorder(gConfig_sistema[i]);
    }
 
    for(var i = 0; i < gConfig_alarmes.length; i++){
        local_clearBorder(gConfig_alarmes[i]);
    }
 
    for(var i = 0; i < gConfig_baterias.length; i++){
        local_clearBorder(gConfig_baterias[i]);
    }
}
//-------------------------------------------------------------------------------------------------    

function config_updateAlarmValues(vccInputFlag)
{
    let TB = [85,   238,  1,  " (V)"];
    let TA = [194,  290,  1,  " (V)"];

    gConfScr_vccInFlag = 0;

    if(vccInputFlag){
        TB = [96,   150,  1,  " (V)"];
        TA = [120,  400,  1,  " (V)"];
        gConfScr_vccInFlag = 1;
    }

    gConfScr_limitValues["Alarmes"][4] = TB;
    gConfScr_limitValues["Alarmes"][5] = TA;
    config_fillTooltip();
}
//-------------------------------------------------------------------------------------------------    

function configUpdateFlags()
{
    let v = [gConfig_baterias[5],gConfig_baterias[6]];
    let y = [gConfig_baterias[4]];
    let w = [gConfig_baterias[7]];
    xpsDisable(v);
    xpsDisable(w);
    xpsDisable(y);

    if(gConfig_USCC[1].checked == true){
        xpsEnable(v);
    }
    
    if(gConfig_USCC[2].checked == true){
        xpsEnable(w);
    }

    if(gConfig_USCC[4].checked == true){
        xpsEnable(y);
    }

    
}
//-------------------------------------------------------------------------------------------------

function local_verifyLimits(val, low, hi)
{
    if(isNaN(val)){
        return "Erro em formato do campo (1).";
    }

    if(val > hi){
        return "Valor mais alto que o permitido.";
    }
    
    if(val < low){
        return "Valor mais baixo que o permitido";
    }
    
    return 0;
}
//-------------------------------------------------------------------------------------------------


function config_fillTooltip()
{
    let i;
    let toolIndex;
    let limits;
    let alarmeLimits;
    let bateriaLimits;
    
    alarmeLimits  = gConfScr_limitValues["Alarmes"];
    bateriaLimits = gConfScr_limitValues["Baterias"];
    toolIndex=0

    // Alarmes
    for(i=0; i<6; i++){
        limits = alarmeLimits[i];
        gXPS_tooltip[toolIndex++].innerHTML = (limits[0]/limits[2]).toFixed(1) + " à " + (limits[1]/limits[2]).toFixed(1) + limits[3];
    }

    // Baterias
    for(i=0; i<8; i++){
        limits = bateriaLimits[i];
        gXPS_tooltip[toolIndex++].innerHTML = (limits[0]/limits[2]).toFixed(1) + " à " + (limits[1]/limits[2]).toFixed(1) + limits[3];
    }
}
//-------------------------------------------------------------------------------------------------

function local_getConfigValues() 
{
    let retArray = [];
    let systype = gConfig_sistema[0].selectedIndex + (gXps_sysUrTy==1?2:0);
    let maxURs = gConfScr_ursPerSystem[systype];

    if(gConfScr_vccInFlag){
        maxURs = 4;
    }

    //-----------------
    // Flags e selects
    //-----------------
    retArray.push(gConfig_USCC[0].getAttribute("value"));
    retArray.push(gConfig_USCC[1].checked?1:0);
    retArray.push(gConfig_USCC[2].checked?1:0);
    retArray.push(gConfig_USCC[3].checked?1:0);
    retArray.push(gConfig_USCC[4].checked?1:0);
    
    // System Type
    retArray.push(parseFloat(gConfig_sistema[0].selectedIndex + (gXps_sysUrTy==1?2:0)));

    // Get Battery type
    retArray.push(parseFloat(gConfig_sistema[4].selectedIndex));

    // Number of URs
    valueRead = parseFloat(gConfig_sistema[1].value);
    if(valueRead > maxURs){
        if(gConfScr_vccInFlag == 0){
            xpsAlert("Número máximo de URs excedido.");
        }
        else{
            xpsAlert("Número máximo de UCs excedido.");
        }
        gConfig_sistema[1].classList.add('inputErrorBorder');
        return [];
    }
    retArray.push(valueRead);

    // Shunt
    retArray.push(parseFloat(gConfig_sistema[2].value));

    //---------
    // Alarmes
    //---------
    for(i=0; i<6; i++){
        let limits = gConfScr_limitValues["Alarmes"][i];

        try{
            valueRead = parseFloat(gConfig_alarmes[i].value) * limits[2];
        }
        catch(err) {
            xpsAlert("Erro em formato do campo (2).");
            gConfig_alarmes[i].classList.add('inputErrorBorder');
            return [];
        }

        ret = local_verifyLimits(valueRead, limits[0], limits[1]);
        if(ret){
            xpsAlert(ret);
            gConfig_alarmes[i].classList.add('inputErrorBorder');
            return [];
        }
        retArray.push(valueRead);
    }

    //----------
    // Baterias
    //----------
    for(i=0; i<8; i++){
        let limits = gConfScr_limitValues["Baterias"][i];
        try{
            valueRead = parseFloat(gConfig_baterias[i].value) * limits[2];
        }
        catch(err) {
            xpsAlert("Erro em formato do campo (2).");
            gConfig_baterias[i].classList.add('inputErrorBorder');
            return [];
        }

        ret = local_verifyLimits(valueRead, limits[0], limits[1]);
        if(ret){
            xpsAlert(ret);
            gConfig_baterias[i].classList.add('inputErrorBorder');
            return [];
        }
        
        retArray.push(valueRead);
    }

    return retArray;
}
//-------------------------------------------------------------------------------------------------

function config_setSysType(typ)
{
    // typ == 0 -> UR Analog
    // typ == 1 -> UR CAN
    // typ == 2 -> Vcc 

    var analog  = '';
    var can     = '';
    var vccIn   = '';

    analog += '<option selected="">SRX 40/60</option>';
    analog += '<option>SRX 80/120</option>';
    
    can  += '<option selected>SRXE 110 - CAN</option>';
    can  += '<option>SR 220 - CAN</option>';
    can  += '<option>SR 440 - CAN</option>';
    can  += '<option>SR 660 - CAN</option>';
    can  += '<option>SRXE 850 - CAN</option>';
    can  += '<option>SRXE 275 - CAN</option>';
    can  += '<option>SRXE 550 - CAN</option>';
    
    vccIn+= '<option selected="">SCX125V/48V/80A</option>';

    if(typ == 0){
        getElid("configsyst").innerHTML = analog;
    }

    if(typ == 1){
        getElid("configsyst").innerHTML = can;        
    }

    if(typ == 2){
        getElid("configsyst").innerHTML = vccIn;        
    }
}
//--------------------------------------------------------------------------------------------------

function config_sysTypeSel()
{
    var idxSel = getElid("configsyst").selectedIndex;
    if((gXps_sysUrTy == 0 ) && (idxSel > 1)){
        return;
    }

    if(gXps_sysUrTy == 1){
        idxSel += 2;
    }

    switch(idxSel){
    case 0:
        // desabilitar a reconexão de bateria; 
        gConfig_sistema[2].value = 350; // Shunt
        gConfig_baterias[7].value = 15; // Current limit
        gConfig_sistema[1].value = 2;   // Number of URs
        break;

    case 1:
        gConfig_sistema[2].value = 600;
        gConfig_baterias[7].value = 20;
        gConfig_sistema[1].value = 4;
        break;
    
    case 2:
        gConfig_sistema[2].value = 600;
        gConfig_baterias[7].value = 20;
        gConfig_sistema[1].value = 2;
        break;

    case 3:
        gConfig_sistema[2].value = 300;
        gConfig_baterias[7].value = 40;
        gConfig_sistema[1].value = 4;
        break;

    case 4:
        gConfig_sistema[2].value = 150;
        gConfig_baterias[7].value = 80;
        gConfig_sistema[1].value = 8;
        break;        

    case 5:
        gConfig_sistema[2].value = 100;
        gConfig_baterias[7].value = 120;
        gConfig_sistema[1].value = 12;
        break;       

    case 6:
        gConfig_sistema[2].value = 100;
        gConfig_baterias[7].value = 120;
        gConfig_sistema[1].value = 15;
        break;        
    
    case 7: //SR 275
        gConfig_sistema[2].value = 300;
        gConfig_baterias[7].value = 120;
        gConfig_sistema[1].value = 5;
        break;        
    
    case 8://SR 550
        gConfig_sistema[2].value = 100; 
        gConfig_baterias[7].value = 120;
        gConfig_sistema[1].value = 10;
        break;        
    }
}
//-------------------------------------------------------------------------------------------------

function config_batTypeSel()
{
    if(getElid("configsysbat").selectedIndex == 0){
        getElid("li-battery").classList.add("menuItem-disabled");
    }
    else{
        getElid("li-battery").classList.remove("menuItem-disabled");        
    }
}
//-------------------------------------------------------------------------------------------------


function config_manCharge()
{
    myHttpPost('configmchr',config_postCallback);
}
//-------------------------------------------------------------------------------------------------

function config_repo()
{
    myHttpPost('configrepo',config_repoCallback);
}
//-------------------------------------------------------------------------------------------------

function config_read()
{
    config_fillTooltip();
    myHttpGet('config.txt',config_readCallBack);
}
//-------------------------------------------------------------------------------------------------

function config_save()
{
    var timeNow = new Date().getTime();

    config_clearBorders();

    let valArray = local_getConfigValues();
    if(valArray.length != 23){
        pLog("Format error");
        return;
    }

    if(timeNow < gConfig_lastSaveTick + 750){
        return;
    }

    // Save the last time in this function.
    gConfig_lastSaveTick = timeNow;

    // Save USCC name.
    sendstring = "";
    sendstring = sendstring.concat(valArray[0]);
    sendstring = sendstring.concat(',');
    // Save numeric values. 
    for(i=1; i < valArray.length; i++){
        sendstring = sendstring.concat((valArray[i].toString(16)).toUpperCase());
        sendstring = sendstring.concat(',');
    }
    
    // Remove the 2 new values incorporated for the new version
    valArray = valArray.slice(0,valArray.length-2)
    pLog(sendstring);
    myHttpPost('ConfigNew='+ sendstring,config_postCallback);
}
//-------------------------------------------------------------------------------------------------

function config_postCallback(text)
{
    if(text == "OK"){
        xpsAlert("Alterações salvas com sucesso.");
    }
    pLog(text);
    config_read();
}
//-------------------------------------------------------------------------------------------------

function config_repoCallback(text)
{
    if(text == "OK"){
        xpsAlert("Reposição bem sucedida.");
    }
    pLog(text);
    config_read();
}
//-------------------------------------------------------------------------------------------------

function config_readCallBack(text)
{
    pLog(text);

    si = arraySpliter(text);

    config_inputWithBorder(gConfig_USCC[0],si[0]);
    gConfig_USCC[1].checked = parseInt(si[13]); 
    gConfig_USCC[2].checked = parseInt(si[16]); 
    gConfig_USCC[3].checked = parseInt(si[18]);
    gConfig_USCC[4].checked = parseInt(si[22]);
    
    config_inputWithBorder(gConfig_sistema[2], parseInt(si[2]));
    
    config_inputWithBorder(gConfig_alarmes[0], parseFloat(si[3]/10).toFixed(1));
    config_inputWithBorder(gConfig_alarmes[1], parseFloat(si[4]/10).toFixed(1));
    config_inputWithBorder(gConfig_alarmes[2], parseFloat(si[5]/10).toFixed(1));
    config_inputWithBorder(gConfig_alarmes[3], parseFloat(si[6]/10).toFixed(1));
    config_inputWithBorder(gConfig_alarmes[4], parseInt(si[7]));
    config_inputWithBorder(gConfig_alarmes[5], parseInt(si[8]));
    
    config_inputWithBorder(gConfig_baterias[0], parseFloat(si[9]/10).toFixed(1));
    config_inputWithBorder(gConfig_baterias[1], parseFloat(si[10]/10).toFixed(1));
    config_inputWithBorder(gConfig_baterias[2], parseFloat(si[11]/10).toFixed(1));
    config_inputWithBorder(gConfig_baterias[3], parseInt(si[12]));
    config_inputWithBorder(gConfig_baterias[4], parseFloat(si[23]/10).toFixed(1));
    config_inputWithBorder(gConfig_baterias[5], parseInt(si[14])); 
    config_inputWithBorder(gConfig_baterias[6], parseInt(si[15]));
    config_inputWithBorder(gConfig_baterias[7], parseFloat(si[17]/10).toFixed(1)); 

    config_inputWithBorder(gConfig_sistema[1],parseFloat(si[19]).toFixed(0));

    gConfig_sistema[0].selectedIndex = parseFloat(si[20]) - (gXps_sysUrTy==1?2:0);
    gConfig_sistema[3].innerHTML     = si[21]=='0'?'<img class="cargaicon">Ligar Carga Manual':'<img class="cargaicon">Desligar Carga Manual';
    gConfig_sistema[4].selectedIndex = parseInt(si[24]);
    config_batTypeSel();

    configUpdateFlags();
    config_clearBorders();
    
    pLog("OK");
}
//-------------------------------------------------------------------------------------------------
