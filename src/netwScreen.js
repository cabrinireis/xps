var gNetwork_trapEnable = document.querySelectorAll('.check-disab.trapcbx');
var gNetwork_trapAddr   = document.querySelectorAll('.form-control.nettrapipa');
var gNetwork_trapPort   = document.querySelectorAll('.form-control.nettrappor');
var gNetwork_trapComm   = document.querySelectorAll('.form-control.nettrapcom');
var gNetwork_macaddr    = getElid('adressMac');

var mNET = [
    getElid("nethmsdma"),
    getElid("follow-address"),
    getElid("netsntpcheck"),
    getElid("netsmtpcheck"),
    getElid("netsmtptestcheck"),
    getElid("netipv4addr"),
    getElid("netipv4mask"),
    getElid("netipv4gate"),
    getElid("netipv4dns1"),
    getElid("netipv4dns2"),
    getElid("netsntpserv1"),
    getElid("netsntpserv2"),
    getElid("netsmtpaddr"),
    getElid("netsmtpport"),
    getElid("netsmtpemailr"),
    getElid("netsmtpemaild"),
    getElid("netsmtpuser"),
    getElid("netsmtppass"),
];

var mNetIpv6 = [
    getElid("ipv6-auto-address"),
    getElid("ipv6-follow-address"),
    getElid("netipv6addr"),
    getElid("netipv6prefixlen"),
    getElid("netipv6LinkLocal")
];

var gNetwork_httptimeout = getElid("httptimeout");
var gNetwork_snmptimeout = getElid("snmptimeout");

var gNetwScreen_getReqTimer = new Date;

//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

function network_enableTrap(idx)
{
    if(idx >= 3){
        return;
    }

    gNetwork_trapAddr[idx].disabled = false;
    gNetwork_trapPort[idx].disabled = false;
    gNetwork_trapComm[idx].disabled = false;
}
//-------------------------------------------------------------------------------------------------

function network_disableTrap(idx)
{
    if(idx >= 3){
        return;
    }
    gNetwork_trapAddr[idx].disabled = true;
    gNetwork_trapPort[idx].disabled = true;
    gNetwork_trapComm[idx].disabled = true;
}
//-------------------------------------------------------------------------------------------------

function buildAuthMail(user,pass){
	var result = "";
	result+= (String.fromCharCode(0));
	result+= user;
	result+= (String.fromCharCode(0));
	result+= pass;
    return btoa(result);
};
//-------------------------------------------------------------------------------------------------

function netIpv4Enable(e)
{
    v = [mNET[5],mNET[6],mNET[7],mNET[8],mNET[9]];
    xpsDisable(v);
    if(e == true){
        xpsEnable(v);
    }
};
//-------------------------------------------------------------------------------------------------

function netIpv6Enable(e)
{
    v = [mNetIpv6[2]];
    xpsDisable(v);
    if(e == true){
        xpsEnable(v);
    }
};
//-------------------------------------------------------------------------------------------------

function netSntpEnable(e)
{
    v = [mNET[10],mNET[11]];
    xpsDisable(v);
    if(e == true){
        xpsEnable(v);
    }
};
//-------------------------------------------------------------------------------------------------

function netSmtpEnable(e)
{
    v = [mNET[12],mNET[13],mNET[14],mNET[15],mNET[16],mNET[17]];
    xpsDisable(v);
    if(e == true){
        xpsEnable(v);
    }
};
//-------------------------------------------------------------------------------------------------

function btSaveNet()
{
    var s = '';
    var v;
    var ip6StrTest;
    var ip6Regex = /^([0-9A-Fa-f]{4}:){7}([0-9A-Fa-f]{4})$/;
    var httpRest;
    var snmpRest;

    s = s.concat(mNET[1].checked?"0*":"1*");
    s = s.concat(mNET[2].checked?"1*":"0*");
    s = s.concat(mNET[3].checked?"1*":"0*");
    s = s.concat(mNET[4].checked?"1*":"0*");
    for(i=5;i<mNET.length;i++){
        v = mNET[i].value;
        mNET[i].classList.remove('inputChangeBorder');
        s = s.concat((v==""?"0.0.0.0":v) + "*");
    }

    //--------
    //- IPv6 -
    //--------
    s = s.concat(mNetIpv6[0].checked?"1*":"0*");
    v = mNetIpv6[2].value;

    ip6StrTest = ip6Regex.test(v)
    if(ip6StrTest == false){
        xpsAlert("Erro No formato do IPv6");
        return;
    }

    v = v.replace(/:/g, '')
    s = s.concat((v==""?"00000000000000000000000000000000":v) + "*");
    mNetIpv6[2].classList.remove('inputChangeBorder');

    v = mNetIpv6[3].value;
    mNetIpv6[3].classList.remove('inputChangeBorder');
    s = s.concat((v==""?"64":v) + "*");

    //--------
    //- SNMP -
    //--------
    s = s.concat(gNetwork_trapEnable[0].checked?"1*":"0*");
    s = s.concat(gNetwork_trapEnable[1].checked?"1*":"0*");
    s = s.concat(gNetwork_trapEnable[2].checked?"1*":"0*");

    s = s.concat(gNetwork_trapAddr[0].value + '*');
    s = s.concat(gNetwork_trapAddr[1].value + '*');
    s = s.concat(gNetwork_trapAddr[2].value + '*');

    s = s.concat(gNetwork_trapPort[0].value + '*');
    s = s.concat(gNetwork_trapPort[1].value + '*');
    s = s.concat(gNetwork_trapPort[2].value + '*');

    s = s.concat(gNetwork_trapComm[0].value + '*');
    s = s.concat(gNetwork_trapComm[1].value + '*');
    s = s.concat(gNetwork_trapComm[2].value + '*');
    
    local_clearSnmpBorder();

    //-------------
    // HTTP reset
    //-------------    
    s = s.concat(http_rst_chk.checked?'1*':'0*');
    if(http_rst_chk.checked){
        httpRest = gNetwork_httptimeout.value;
        if(httpRest < 5){
            xpsAlert("Reset por HTTP deve ser no mínimo 5 minutos");
            return;
        }
        if((httpRest > 60*24*15)){
            xpsAlert("Reset por HTTP deve ser menor que 15 dias");
            return;
        }
    }
    s = s.concat(gNetwork_httptimeout.value + '*');
    gNetwork_httptimeout.classList.remove('inputChangeBorder');

    //-------------
    // SNTP reset
    //-------------    
    s = s.concat(snmp_rst_chk.checked?'1*':'0*');
    if(snmp_rst_chk.checked){
        snmpRest = gNetwork_snmptimeout.value;
        if(snmpRest < 5){
            xpsAlert("Reset por SNMP deve ser no mínimo 5 minutos");
            return;
        }
        if((snmpRest > 60*24*15)){
            xpsAlert("Reset por SNMP deve ser menor que 15 dias");
            return;
        }
    }
    s = s.concat(gNetwork_snmptimeout.value + '*');
    gNetwork_snmptimeout.classList.remove('inputChangeBorder');

    pLog(s);
    myHttpPost('net+='+ s, btSaveNetCallback);
};
//-------------------------------------------------------------------------------------------------

function btSaveNetCallback(text)
{
    if(text == "OK"){
        xpsAlert("Alterações salvas com sucesso.");
    }
    pLog(text);
};
//-------------------------------------------------------------------------------------------------


function local_clearSnmpBorder()
{
    gNetwork_trapAddr[0].classList.remove('inputChangeBorder');
    gNetwork_trapAddr[1].classList.remove('inputChangeBorder');
    gNetwork_trapAddr[2].classList.remove('inputChangeBorder');

    gNetwork_trapPort[0].classList.remove('inputChangeBorder');
    gNetwork_trapPort[1].classList.remove('inputChangeBorder');
    gNetwork_trapPort[2].classList.remove('inputChangeBorder');

    gNetwork_trapComm[0].classList.remove('inputChangeBorder');
    gNetwork_trapComm[1].classList.remove('inputChangeBorder');
    gNetwork_trapComm[2].classList.remove('inputChangeBorder');
}
//-------------------------------------------------------------------------------------------------

function btReadNet()
{
    var timeNow = new Date;
    if((timeNow - gNetwScreen_getReqTimer) < 500){
        console.log("Calma!!!");
        return;
    }
    gNetwScreen_getReqTimer = new Date;
    myHttpGet('net.txt',btReadNetCallback,5);
};
//-------------------------------------------------------------------------------------------------

function btReadNetCallback(text)
{
    var v = text.split("*");
    var i;

    pLog(v);

    mNET[1].checked = v[1]=="1"?false:true;
    mNET[2].checked = v[2]=="1"?true:false;
    mNET[3].checked = v[3]=="1"?true:false;
    mNET[4].checked = v[4]=="1"?true:false;

    netIpv4Enable(mNET[1].checked);
    netSntpEnable(mNET[2].checked);
    netSmtpEnable(mNET[3].checked);
    

    for(i=5;i<mNET.length;i++){
        mNET[i].value = v[i];
        mNET[i].classList.remove('inputChangeBorder');
    }

    //-----------
    //-- IPv6  --
    //-----------
    mNetIpv6[0].checked = false;
    mNetIpv6[1].checked = false;
    if(v[21]=="1"){
        // DHCPv6 Enabled
        mNetIpv6[0].checked = true;
        netIpv6Enable(false);
    }
    else{
        // DHCPv6 Disable
        mNetIpv6[1].checked = true;        
        netIpv6Enable(true);
    }

    mNetIpv6[2].value = v[22];
    mNetIpv6[2].classList.remove('inputChangeBorder');
    mNetIpv6[3].value = v[23];
    mNetIpv6[3].classList.remove('inputChangeBorder');
    mNetIpv6[4].value = v[24];
    
    //------
    // SNMP
    //------
    gNetwork_trapEnable[0].checked = false; 
    gNetwork_trapEnable[1].checked = false; 
    gNetwork_trapEnable[2].checked = false; 
    network_disableTrap(0);
    network_disableTrap(1);
    network_disableTrap(2);

    if(v[25] == '1'){
        gNetwork_trapEnable[0].checked = true; 
        network_enableTrap(0);
    }

    if(v[26] == '1'){
        gNetwork_trapEnable[1].checked = true; 
        network_enableTrap(1);
    }

    if(v[27] == '1'){
        gNetwork_trapEnable[2].checked = true; 
        network_enableTrap(2);
    }

    gNetwork_trapAddr[0].value = v[28]; 
    gNetwork_trapAddr[1].value = v[29];
    gNetwork_trapAddr[2].value = v[30];

    gNetwork_trapPort[0].value = v[31];
    gNetwork_trapPort[1].value = v[32];
    gNetwork_trapPort[2].value = v[33];

    gNetwork_trapComm[0].value = v[34];
    gNetwork_trapComm[1].value = v[35];
    gNetwork_trapComm[2].value = v[36];

    // HTTP reset
    http_rst_chk.checked = v[37] == "1"?true:false;
    gNetwork_httptimeout.value = v[38];

    // SNMP reset
    snmp_rst_chk.checked = v[39] == "1"?true:false;
    gNetwork_snmptimeout.value = v[40];

    // MAC address
    gNetwork_macaddr.innerHTML = v[41];

    local_clearSnmpBorder();
    gNetwork_httptimeout.classList.remove('inputChangeBorder');
    gNetwork_snmptimeout.classList.remove('inputChangeBorder');

    pLog("OK");
};
//-------------------------------------------------------------------------------------------------

function netSmtpEnableSel()
{
    if(netsmtppass.type == "password"){
        return;
    }
    netFlipShowPass();
}
//-------------------------------------------------------------------------------------------------

function netFlipShowPass()
{
    if(netsmtpcheck.checked == false){
        netsmtppass.setAttribute('type','password');
        sMailPass.classList.add('active');
        return;
    }

    if(netsmtppass.type == "password"){
        netsmtppass.setAttribute('type','text');
        sMailPass.classList.remove('active');
    }
    else{
        netsmtppass.setAttribute('type','password');
        sMailPass.classList.add('active');
    }
};
//-------------------------------------------------------------------------------------------------

function local_trapEnState(idx)
{
    if(idx >= 3){
        return;
    }
    network_disableTrap(idx);
    if(gNetwork_trapEnable[idx].checked == true){
        network_enableTrap(idx);
    }
}
//-------------------------------------------------------------------------------------------------


function netInit()
{
    // IPv6 Prefix is fixed in 64
    v = [mNetIpv6[3]];
    xpsDisable(v);

    // Radio Button - IPv4 DHCP Enable
    getElid("auto-address").addEventListener("change", function(){ netIpv4Enable(false);  });
    // Radio Button - IPv4 DHCP Disable
    getElid("follow-address").addEventListener("change", function(){ netIpv4Enable(true); });
    // Radio Button - IPv6 DHCP Enable
    getElid("ipv6-auto-address").addEventListener("change", function(){ netIpv6Enable(false); });
    // Radio Button - IPv6 DHCP Disable
    getElid("ipv6-follow-address").addEventListener("change", function(){ netIpv6Enable(true); });

    mNET[2].addEventListener("change", function(){
        netSntpEnable(false);
        if(this.checked){
            netSntpEnable(true);
        }
    });

    mNET[3].addEventListener("change", function(){
        netSmtpEnable(false);
        if(this.checked){
            netSmtpEnable(true);
        }
    });

    // Trap 1
    gNetwork_trapEnable[0].addEventListener("change",function(){ local_trapEnState(0); });
    // Trap 2
    gNetwork_trapEnable[1].addEventListener("change",function(){ local_trapEnState(1); });
    // Trap 3
    gNetwork_trapEnable[2].addEventListener("change",function(){ local_trapEnState(2); });
};
//-------------------------------------------------------------------------------------------------

netInit();