var  monGif =  getElid("mcirc");

var gMonit_urTable = getElid("monit_ur_table");
var gMonit_urSta = document.querySelectorAll('.filterable-cell.sta');
var gMonit_urCur = document.querySelectorAll('.filterable-cell.cur');
var gMonit_urVca = document.querySelectorAll('.filterable-cell.vca');

var gMonit_batteryData = document.querySelectorAll('.monitt.batte');
var gMonit_systemData = document.querySelectorAll('.monitt.systt');
var gMonit_infraData = [getElid("mtep1"),getElid("mtep2")];

var gMonit_alrmPan  =[getElid("mpan1"), getElid("mpan2"), getElid("mpan3")];
var gMonit_alrmBat  =[getElid("mbatdes"), getElid("mbatemd"), getElid("mbatemc"), getElid("mbatcap"), getElid("mbatsim")];
var gMonit_alrmSys  =[getElid("msisvcc"),getElid("msisfca"),getElid("msisfus"),getElid("msisfur")];
var gMonit_infraInp =[getElid("min1"),getElid("min2"),getElid("min3"),getElid("min4"),getElid("min5")];
var gMonit_infraOut =[getElid("mout1"),getElid("mout2"),getElid("mout3"),getElid("mout4"),getElid("mout5"),getElid("mout6"),getElid("mout7")];
var gMonit_infraTmp =[getElid("monTmp1"),getElid("monTmp2")];

var gMonit_infraInpLb =[getElid("minnm1"),getElid("minnm2"),getElid("minnm3"),getElid("minnm4"),getElid("minnm5")];
var gMonit_infraOutLb =[getElid("moutnm1"),getElid("moutnm2"),getElid("moutnm3"),getElid("moutnm4"),getElid("moutnm5"),getElid("moutnm6"),getElid("moutnm7")];
var gMonit_infraTmpLb =[getElid("mtepnm1"),getElid("mtepnm2")];

var gMonit_verC = 0;

function monCircRemoveClass() 
{
    monGif.classList.remove("circuit000","circuit001","circuit010","circuit011","circuit100","circuit101","circuit110","circuit111");
};
//-------------------------------------------------------------------------------------------------

function monTurnOffAllLeds() 
{
    // Panel Leds
    gMonit_alrmPan[0].setAttribute('class','deadyellow-circle');
    gMonit_alrmPan[1].setAttribute('class','deadyellow-circle');
    gMonit_alrmPan[2].setAttribute('class','deadred-circle');
    // Battery LEDs
    gMonit_alrmBat[0].setAttribute('class','deadred-circle');
    gMonit_alrmBat[1].setAttribute('class','deadred-circle');
    gMonit_alrmBat[2].setAttribute('class','deadred-circle');
    gMonit_alrmBat[3].setAttribute('class','deadred-circle');
    gMonit_alrmBat[4].setAttribute('class','deadred-circle');
    // System LEDs
    gMonit_alrmSys[0].setAttribute('class','deadred-circle');
    gMonit_alrmSys[1].setAttribute('class','deadred-circle');
    gMonit_alrmSys[2].setAttribute('class','deadred-circle');
    gMonit_alrmSys[3].setAttribute('class','deadred-circle');
    // Inputs LEDs
    gMonit_infraInp[0].setAttribute('class','deadred-circle');
    gMonit_infraInp[1].setAttribute('class','deadred-circle');
    gMonit_infraInp[2].setAttribute('class','deadred-circle');
    gMonit_infraInp[3].setAttribute('class','deadred-circle');
    gMonit_infraInp[4].setAttribute('class','deadred-circle');
    // Output LEDs 
    gMonit_infraOut[0].setAttribute('class','deadred-circle');
    gMonit_infraOut[1].setAttribute('class','deadred-circle');
    gMonit_infraOut[2].setAttribute('class','deadred-circle');
    gMonit_infraOut[3].setAttribute('class','deadred-circle');
    gMonit_infraOut[4].setAttribute('class','deadred-circle');
    gMonit_infraOut[5].setAttribute('class','deadred-circle');
    gMonit_infraOut[6].setAttribute('class','deadred-circle');
    // Temperature Leds
    gMonit_infraTmp[0].setAttribute('class','deadred-circle');
    gMonit_infraTmp[1].setAttribute('class','deadred-circle');
};
//-------------------------------------------------------------------------------------------------

function local_batteryPanel(arrayIN)
{
    arrayIN[0] = parseInt(arrayIN[0],16); // V BAT 1
    arrayIN[1] = parseInt(arrayIN[1],16); // I BAT 1
    arrayIN[2] = parseInt(arrayIN[2],16); // T BAT 1
    

    tt = arrayIN[1];
    if(tt & 0x8000){
        tt = 0x10000 - tt;
        tt *= -1;
    }
    arrayIN[1] = tt;

    arrayIN[0] = (arrayIN[0]/10).toFixed(1) + " V";
    arrayIN[1] = (arrayIN[1]/10).toFixed(1) + " A";
    arrayIN[2] = arrayIN[2]==65534?'Invertido':arrayIN[2]==65535?'Sem Sen':arrayIN[2];

    return arrayIN;
};
//-------------------------------------------------------------------------------------------------

function local_systemPanel(arrayIN)
{
    arrayIN[0] = parseInt(arrayIN[0],16); // V BAT 1
    arrayIN[1] = parseInt(arrayIN[1],16); // I BAT 1
  
    tt = arrayIN[1];
    if(tt & 0x8000){
        tt = 0x10000 - tt;
        tt *= -1;
    }
    arrayIN[1] = tt;

    arrayIN[0] = (arrayIN[0]/10).toFixed(1) + " V";
    arrayIN[1] = (arrayIN[1]/10).toFixed(1) + " A";

    return arrayIN;
};
//-------------------------------------------------------------------------------------------------

function monit_buildUrTable(arrayIn)
{
    var urTabHead =  getElid("moniturtyp");
    let tableStr = '';
    let numLines;
    let urType;
    let i = 0;
    
    urType   = parseInt(arrayIn[0],16);
    numLines = parseInt(arrayIn[1],16);
    arrayIn  = arrayIn.splice(2); // Remove first and second elements

    urTabHead.innerHTML = "UR";
    switch(urType){
    case 0:
        urTabHead.innerHTML = "UR AN"; // Analogica
        break;
    case 1:
        urTabHead.innerHTML = "UR AE"; // DPC
        break;
    case 2:
        urTabHead.innerHTML = "UR HE"; // Impow
        break;
    case 3:
        urTabHead.innerHTML = "UR VV"; // Vertivi
        break;
    }
    
    if (gMonit_verC){
        urTabHead.innerHTML = "UC";
    }

    for(i=0; i<numLines; i++){
        arrayIn[0]  = parseInt(arrayIn[0],16);  // UR status
        arrayIn[1]  = parseInt(arrayIn[1],16);  // UR Current
        arrayIn[2]  = parseInt(arrayIn[2],16);  // UR VCA
        
        arrayIn[0]  = arrayIn[0]==0?"Ausente":arrayIn[0]==1?"OK":"Falha";
        arrayIn[1] = (arrayIn[1]/10).toFixed(1) + " A";

        tableStr +=  '<tr>';
        tableStr += '<td class="filterable-cell">' + (i+1) + '</td>';
        tableStr += '<td class="filterable-cell">' + arrayIn[0] + '</td>';
        tableStr += '<td class="filterable-cell">' + arrayIn[1] + '</td>';
        tableStr += '<td class="filterable-cell">' + arrayIn[2] + '</td>';
        tableStr += '</tr>'

        arrayIn  = arrayIn.splice(3); // Remove 3 alements element
    }
    monit_ur_table.innerHTML = tableStr;
}
//-------------------------------------------------------------------------------------------------

function monCirc(cod){
    monCircRemoveClass();
    switch(cod){
        case "000":
            monGif.classList.add("circuit000");
            break;
        case "001":
            monGif.classList.add("circuit001");
            break;
        case "010":
            monGif.classList.add("circuit010");
            break;
        case "011":
            monGif.classList.add("circuit011");
            break;
        case "100":
            monGif.classList.add("circuit100");
            break;
        case "101":
            monGif.classList.add("circuit101");
            break;
        case "110":
            monGif.classList.add("circuit110");
            break;
        case "111":
            monGif.classList.add("circuit111");
            break;
        default:
            monGif.classList.add("circuit000");
    }
};
//-------------------------------------------------------------------------------------------------

function monit_setInputLabels(idx,label)
{
    if(idx >= gMonit_infraInpLb.length){
        return;
    }
    gMonit_infraInpLb[idx].innerHTML = label;
};
//-------------------------------------------------------------------------------------------------

function monit_setOutputLabels(idx,label)
{
    if(idx >= gMonit_infraOutLb.length){
        return;
    }
    gMonit_infraOutLb[idx].innerHTML = label;
};
//-------------------------------------------------------------------------------------------------

function monit_setTemperaLabels(idx,label)
{
    if(idx >= gMonit_infraTmpLb.length){
        return;
    }
    gMonit_infraTmpLb[idx].innerHTML = label;
};
//-------------------------------------------------------------------------------------------------

function getMonitCurrentValues()
{
    myHttpGet('monCur.txt',monCurCallback);
};
//-------------------------------------------------------------------------------------------------

function monCurCallback(text)
{
    pLog(text);
   
    let retArr;
    let panels = text.split(';');
    let fwVerStr = " ";

    if (panels.length != 7){
        pLog("Format error (1)");
        return;
    }

    //------------
    //-  Figure  -
    //------------
    monCirc(panels[0]);

    //------------
	//- Baterias -
	//------------
    retArr = local_batteryPanel(panels[1].split(',')); 
    gMonit_batteryData[0].innerHTML = retArr[0]; // VBAT
    gMonit_batteryData[1].innerHTML = retArr[1]; // IBat
    gMonit_batteryData[2].innerHTML = retArr[2]; // TBAT
	
    //------------
	//- Sistema  -
	//------------
    retArr = local_systemPanel(panels[2].split(','));
    gMonit_systemData[0].innerHTML = retArr[0]; // Vcc
    gMonit_systemData[1].innerHTML = retArr[1]; // IURS

    //---------
	//-  URs  -
	//---------
    monit_buildUrTable(panels[3].split(','));
    
	//-----------
	//- Alarmes - 4
	//-----------
    monTurnOffAllLeds(); 
    retArr = panels[4].split(',');
    // Panel LEDs
    if(retArr[0]  == '1'){gMonit_alrmPan[0].setAttribute('class','yellow-circle');}
    if(retArr[1]  == '1'){gMonit_alrmPan[1].setAttribute('class','yellow-circle');}
    if(retArr[2]  == '1'){gMonit_alrmPan[2].setAttribute('class','red-circle');}
    // Battery LEDs
    if(retArr[3]  == '1'){gMonit_alrmBat[0].setAttribute('class','red-circle');}
    if(retArr[4]  == '1'){gMonit_alrmBat[1].setAttribute('class','red-circle');}
    if(retArr[5]  == '1'){gMonit_alrmBat[2].setAttribute('class','red-circle');}
    if(retArr[6]  == '1'){gMonit_alrmBat[3].setAttribute('class','red-circle');}
    if(retArr[7]  == '1'){gMonit_alrmBat[4].setAttribute('class','red-circle');}
	// System LEDs
    if(retArr[8]  == '1'){gMonit_alrmSys[0].setAttribute('class','red-circle');}
    if(retArr[9]  == '1'){gMonit_alrmSys[1].setAttribute('class','red-circle');}
    if(retArr[10] == '1'){gMonit_alrmSys[2].setAttribute('class','red-circle');}
    if(retArr[11] == '1'){gMonit_alrmSys[3].setAttribute('class','red-circle');}

	//------------------
	//- Infraestrutura -
	//------------------
    retArr = panels[5].split(',');
    // Inputs LEDs
    if(retArr[0]  == '1'){gMonit_infraInp[0].setAttribute('class','red-circle');}
    if(retArr[1]  == '1'){gMonit_infraInp[1].setAttribute('class','red-circle');}
    if(retArr[2]  == '1'){gMonit_infraInp[2].setAttribute('class','red-circle');}
    if(retArr[3]  == '1'){gMonit_infraInp[3].setAttribute('class','red-circle');}
    if(retArr[4]  == '1'){gMonit_infraInp[4].setAttribute('class','red-circle');}
    // Output LEDs
    if(retArr[5]  == '1'){gMonit_infraOut[0].setAttribute('class','red-circle');}
    if(retArr[6]  == '1'){gMonit_infraOut[1].setAttribute('class','red-circle');}
    if(retArr[7]  == '1'){gMonit_infraOut[2].setAttribute('class','red-circle');}
    if(retArr[8]  == '1'){gMonit_infraOut[3].setAttribute('class','red-circle');}
    if(retArr[9] == '1'){gMonit_infraOut[4].setAttribute('class','red-circle');}
    if(retArr[10] == '1'){gMonit_infraOut[5].setAttribute('class','red-circle');}
    if(retArr[11] == '1'){gMonit_infraOut[6].setAttribute('class','red-circle');}

    // Temperature LEDs
    if(retArr[13] == '1'){gMonit_infraTmp[0].setAttribute('class','red-circle');}
    if(retArr[14] == '1'){gMonit_infraTmp[1].setAttribute('class','red-circle');}

    // Tamb 1. Tamb 2
    retArr[15] = parseInt(retArr[15],16); // Tamb 1
    retArr[15] = retArr[15]==65534?'Invertido':retArr[15]==65535?'Sem Sen':retArr[15];
    retArr[16] = parseInt(retArr[16],16); // Tamb 2
    retArr[16] = retArr[16]==65534?'Invertido':retArr[16]==65535?'Sem Sen':retArr[16];

    gMonit_infraData[0].innerHTML = retArr[15]; 
    gMonit_infraData[1].innerHTML = retArr[16]; 

	//------------------
	//-  Other Things  -
	//------------------
    retArr = panels[6].split(',');
    // Lithium enabled
    getElid("li-battery").classList.remove("menuItem-disabled");
    if(retArr[0] == '0'){
        getElid("li-battery").classList.add("menuItem-disabled");
    }

    // URs types
    gXps_sysUrTy = retArr[1] == '0'?0:retArr[1] == '1'?1:2;
    config_setSysType(gXps_sysUrTy);

    // fwVersion
    fwVerStr = retArr[2];
    getElid('fwVersion').innerHTML = "FW Version: " + fwVerStr;

    // Change according to version
    gMonit_verC = 0;    
    config_updateAlarmValues(0);
    if(fwVerStr[0] == 'C'){
        gMonit_verC = 1;
        config_updateAlarmValues(1);
        getElid('monitvintp0').innerHTML = "VCCin";
        getElid('monitvintp1').innerHTML = "Falha VCCin";
        getElid('monitvintp2').innerHTML = "Falha UC";
        getElid('monitvintp3').innerHTML = "Falha VCCin";
        getElid('monitvintp4').innerHTML = "Falha UC";
        getElid('monitvintp5').innerHTML = "Tensão VCCin Baixa";
        getElid('monitvintp6').innerHTML = "Tensão VCCin Alta";
        getElid('confnumurslb').innerHTML = "Número de UCs";
        hist_setVccInputStrings();
    }
};
//-------------------------------------------------------------------------------------------------
