window.getElid   = function (id) {return document.getElementById(id);};


var gMyDebugLink = 0;
var gDebugHost   = '192.168.1.200';
var gDebugPort   = '80';
var gAuthToken   = null;
var gUriGetNor   = null;
var gUriGetDbg   = null;
var gUriPostNor  = null;
var gUriPostDbg  = null;

var enter               = getElid("gobtn");
var loginfail           = getElid("loginfail");
var gConectionTimeout   = 10;

String.prototype.hashCode = function()
{
    var hash = 0;
    if (this.length == 0) return 0;
    for (i = 0; i < this.length; i++) {
        char = this.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
    }
    if(hash < 0){
        hash = -hash
    }
    while(hash>0xffffffff){
        hash /= 1.023;
    }
    return hash.toFixed();
};
//-------------------------------------------------------------------------------------------------

function pLog(text)
{
    if(gMyDebugLink){
        console.log(text);
    }
};
//-------------------------------------------------------------------------------------------------

function local_resetTimer()
{
    gConectionTimeout = 10;
};
//-------------------------------------------------------------------------------------------------

function local_getAuth()
{
    gAuthToken = sessionStorage.getItem('aUth');
    if(gAuthToken == null){
        gAuthToken = "00000000000000";
    }
    gUriGetNor   = 'http://' + window.location.hostname + ':' + window.location.port + '/'+ gAuthToken + '?';
    gUriGetDbg   = 'http://' + gDebugHost + ':' + gDebugPort + '/'+ gAuthToken + '?';
    gUriPostNor  = gUriGetNor + 'post.txt?';
    gUriPostDbg  = gUriGetDbg + 'post.txt?';
}
//-------------------------------------------------------------------------------------------------

function myHttpGet(file,myCallback){
    if(myCallback == null){
        console.log("No Callback Found");
        return;
    }

    local_getAuth();
    if(gMyDebugLink == 1){
        localUri = gUriGetDbg + file;
    }
    else{
        localUri = gUriGetNor + file
    }

    localUri += '?ti='+ new Date().getTime();
    pLog(localUri);

    var req = new XMLHttpRequest();
    req.timeout = 3000;
    req.open('GET',localUri, true);
    req.onreadystatechange = function(){
        if(req.readyState == 4 && req.status == 200){
            xpsHttpGetCallB("OK:" + req.responseText,myCallback);
            local_resetTimer();
        }
        if(req.status == 0){
            xpsHttpGetCallB("No response.",null);
        }
    };
    req.send();
};
//-------------------------------------------------------------------------------------------------

function verifyPermitedPost(textIn)
{
    a = textIn.search('log1n.txt?');
    b = textIn.search('NeWPaSs.txt');

    if(a!=-1 || b!=-1){
        return 1;
    }
    return 0;
};
//-------------------------------------------------------------------------------------------------

function myHttpPost(uriOut,myCallback){
    if(myCallback == null){
        pLog("No Callback Found");
        return;
    }

    level = sessionStorage.getItem('Level');
    if(level != '3'){
        l = verifyPermitedPost(uriOut);
        if(l==0){
            xpsAlert("Esse usuário não tem permissão de escrita.");
            return;
        }
    }

    local_getAuth();
    if(gMyDebugLink == 1){
        localUri = gUriPostDbg + uriOut;
    }
    else{
        localUri = gUriPostNor + uriOut;
    }
    pLog(localUri);

    var req = new XMLHttpRequest();
    req.timeout = 3000;
    req.open('GET',localUri, true);
    req.onreadystatechange = function(){
        if(req.readyState == 4 && req.status == 200){
            xpsHttpGetCallB("OK:" + req.responseText,myCallback);
            local_resetTimer();
        }
        if(req.status == 0){
            xpsHttpGetCallB("No response.",null);
        }
    };
    req.send();
};
//-------------------------------------------------------------------------------------------------

function xpsHttpGetCallB(text, cb)
{
    msgIdx = text.indexOf(':');
    if(msgIdx < 0){
        pLog("No response.");
        return;
    }
    if(cb == null){
        pLog("Pack unknouwn.");
        return;
    }
    cb(text.slice(msgIdx+1));
}
//-------------------------------------------------------------------------------------------------

function loginVal(event)
{
    sessionStorage.setItem('Level','0');
    event.preventDefault();
    var user = getElid("user").value;
    var pswr = getElid("pass").value.hashCode();
    sessionStorage.clear();
    sessionStorage.setItem('uSeR', user);
    sessionStorage.setItem('pasSwD', pswr);
    myHttpPost("log1n.txt?" + user + "?" + pswr, loginValCallB);
};
//-------------------------------------------------------------------------------------------------

function loginValCallB(textIn)
{
    sessionStorage.setItem('Level','0');
    textIn = textIn.split(':')
    if(textIn.length < 2){
        console.log("Error(1).");
        return;
    }

    if (textIn[0] == "NO"){
        enter.setAttribute("href","#");
        loginfail.innerHTML = 'O usuário ou a senha não estão corretos!';
        return;
    }

    if (textIn[0] == "OK"){
        console.log("Login OK.");
        loginfail.innerHTML = "";
        sessionStorage.setItem('aUth', textIn[1]);
        sessionStorage.setItem('Level',textIn[2]);
        if(gMyDebugLink){
            document.location.href = "dashboard.html";    
        }
        else{
            document.location.href = textIn[1]+'?';
        }
        return;
    }
    console.log("Error(2).");
}
//-------------------------------------------------------------------------------------------------